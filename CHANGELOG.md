## 3.579.0 (2025-01-10)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.579.0)

*  [BUGFIX][SPC-4768] Added patch to fix possibility to add empty products via bulk api *(Martijn)*
*  [BUGFIX][SPC-4768] Added patch to fix possibility to add empty products via bulk api *(Martijn)*


## 3.578.0 (2025-01-10)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.578.0)

*  [FEATURE][EHP-245] added multimatch to text attributes *(Lewis Voncken)*
*  patches/0578_ change_text_attribute_filter_input_type_graphql.patch edited online with Bitbucket *(Lewis Voncken)*
*  [BUGFIX][SPC-4768] Added patch to fix possibility to add empty products via bulk api *(Martijn)*


## 3.577.0 (2024-12-18)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.577.0)

*  [FEATURE][DBD-50]Add AmPmMarkersAbbr as falback to AmPmMarkers, as done in magento in https://github.com/magento/magento2/commit/144b491c7720a983c33c7c20b1f77ccd559c1031 *(Simon Vianen)*
*  [FEATURE][HHP-298] Patch to fix bug to add product with a customizable option to cart *(Jeffrey Leeuw)*


## 3.576.0 (2024-11-05)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.576.0)

*  [FEATURE][VKT-842] Added patch to allow enable cache for countries query to speed up checkout which is default in 2.4.7 *(Lewis Voncken)*


## 3.571.0 (2024-10-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.571.0)

*  570_CVE-2024-34102_cosmic_sting_hotfix.patch created online with Bitbucket *(Matthijs Breed)*
*  0572_CVE-2024-34102_cosmic_sting_hotfix.patch created online with Bitbucket *(Matthijs Breed)*
*  0573_Cosmic_Sting_patch_framework_2.4.4-5. created online with Bitbucket *(Matthijs Breed)*
*  0573_Cosmic_Sting_patch_framework_2.4.6.patch created online with Bitbucket *(Matthijs Breed)*
*  0575_Cosmic_Sting_patch_framework_2.4.7.patch created online with Bitbucket *(Matthijs Breed)*
*  patches/0573_Cosmic_Sting_patch_framework_2.4.4-5. edited online with Bitbucket *(Matthijs Breed)*
*  patches/0573_Cosmic_Sting_patch_framework_2.4.6.patch edited online with Bitbucket *(Matthijs Breed)*
*  0574_Cosmic_Sting_patch_framework_2.4.6.patch edited online with Bitbucket *(Matthijs Breed)*
*  [FEATURE][HDVS-377]fixed color being out of range for imagecolorsforindex in Gd2 *(simon vianen)*


## 3.570.0 (2024-05-01)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.570.0)

*  [FEATURE][HDVS-146] error_rp_token_when_save_customer *(Jeffrey Leeuw)*
*  [FEATURE][HDVS-146] error_rp_token_when_save_customer *(Jeffrey Leeuw)*
*  [FEATURE][DAO-316] added patch for fixing positioning of datepicker in admin ui *(Dennis Frijlink)*
*  [FEATURE][DAO-316] renamed patch for fix positioning datepicker *(Dennis Frijlink)*
*  [BUGFIX][DDCS-210]fix limiter to change for 2.4.6 *(EdwinDeHaas)*
*  [FEATURE][CHRISP-466] added patch for displaying the shipping price including tax in the graphql *(Thomas Mondeel)*


## 3.569.0 (2023-09-12)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.569.0)

*  updated file with deprication on version 2.4.3-p1 and the amasty/base 2.17.2 *(Thomas Mondeel)*
*  [FEATURE][DN-311] fix_adyen_cancel_payment *(Jeffrey Leeuw)*
*  [DOCS] Updated the CHANGELOG.md *(Jeffrey Leeuw)*
*  [FEATURE][DSORG-1410] Added patch to fix customer custom attribute not unsetting on save *(Quinn Stadens)*
*  [DOCS] Updated the CHANGELOG.md *(Quinn Stadens)*
*  [FEATURE][DSORG-1421] Fixed configprovider of captcha having incorrect construct *(Quinn Stadens)*
*  [FEATURE][SWBT-196] - Fix bulk async api products string array bug *(Ruben Panis)*
*  [FEATURE][SMSE-558] *(Jeffrey Leeuw)*
*  [BUGFIX] Fixed Creditmemo's breaking when free shipment *(René Schep)*
*  [FEATURE][DAO-188] Dissable quantity_and_stock_status logging *(Alexander Franke)*
*  [FEATURE][DAO-188] Fixed diff typo *(Alexander Franke)*
*  [FEATURE][RM-856] Add patch to add aajx param to elasticsuite ajax update *(Jordy Pouw)*


## 3.563.0 (2023-06-14)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.563.0)

*  [FEATURE][SONPU-629] fixing admin password reset link *(Jeffrey Leeuw)*
*  [DOCS] Updated the CHANGELOG.md *(Jeffrey Leeuw)*
*  0560_fix_sitemap_generation_empty_image_title_2.4.6_CE_COMPOSER.patch created online with Bitbucket *(Matthijs Breed)*
*  [BUGFIX][GSS-853] Add leading zero to patch file name according to convention *(Jordy Pouw)*
*  [FEATURE][SFIN-857] Add patch for incorrect jquery ui modules paths slider & selectmenu 2.4.4 & 2.4.5 *(Jordy Pouw)*


## 3.562.0 (2023-05-17)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.562.0)

*  [FEATURE][SDP-970] Add 0556_Magento_framework_db_schema_index.patch created online with Bitbucket *(Matthijs Breed)*
*  [BUGFIX][SGO-734] - Add patch to prevent pipeline error on inventory legacy stock patch *(Ruben Panis)*
*  [BUGFIX][SGO-734] - Fix patch *(Ruben Panis)*
*  [BUGFIX][SGO-734] - Extend patch with similar issues *(Ruben Panis)*
*  <[BUGFIX|FEATURE][DOR-955]Fixed error when exporting cutomer grid to csv> *(Taco Jongkind)*


## 3.561.0 (2023-05-10)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.561.0)

*  [FEATURE][SCSUP-1899] Remove /admin from storemanager url *(FabianBank)*


## 3.560.0 (2023-05-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.560.0)

*  [FEATURE][SGO-245] stockitemconfigurations_strict_typing *(Jeffrey Leeuw)*
*  [BUGFIX][NIJS-198] - Fix empty productlist widget cache *(Ruben Panis)*
*  [DOCS] Updated the CHANGELOG.md *(Ruben Panis)*
*  [FEATURE][PPC-1045] Prevent areacode from being overridden *(exp0219)*
*  [FEATURE][PPC-1045] Rename patch to add number in front *(exp0219)*
*  [FEATURE] [M2FIX-45] stockitemconfigurations_strict_typing *(Pascal van Gelder)*
*  [FEATURE][SGO-649] - Keep other filtervalues when using category filter and config value smile_elasticsuite_catalogsearch_settings/catalogsearch/category_filter_use_url_rewrites is set to Yes *(Ruben Panis)*
*  [FEATURE][PPC-1045] Fix error in patch *(exp0219)*
*  [FEATURE][PPC-1045] Change patch name *(exp0219)*


## 3.550.0 (2023-03-31)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.550.0)

*  [FEATURE][GSS-853] Removed caching from crosssell products anowave ec module *(Jitske)*


## 3.449.0 (2023-03-28)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.449.0)

*  [FEATURE][M2FIX-35] Updated readme to explain how to use phpstorm to create patches easily, added Patch to fix null customer groups *(René Schep)*


## 3.448.0 (2023-03-14)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.448.0)

*  [FEATURE][DSORG-1073] Remove patch since it was added to app/code *(exp0219)*
*  [FEATURE][VKPW-106] added wishlist patch for magento 2.4.2 *(Dylan Maurits)*


## 3.446.2 (2023-02-23)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.446.2)

*  [FEATURE][SUPRO-1073] Fix file extension compatibility issues causing cropper to break *(exp0219)*


## 3.446.1 (2023-02-20)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.446.1)

*  [BUGFIX][AOM2-508] Fix patch 0447 *(Willem König)*


## 3.446.0 (2023-02-20)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.446.0)

*  [BUGFIX][AOM2-508] Add patch for 'invalid security or form key' upon admin login in 2.4.2 *(Willem König)*


## 3.445.0 (2023-02-14)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.445.0)

*  0287_fix_admin_session_2.4.3_COMPOSER.patch [M2FIX-1] *(Pascal van Gelder)*
*  [FEATURE][DDONS-338] rewrited patch 0237 for Magento 245p1 *(martijn)*
*  [DOCS] Updated the CHANGELOG.md *(martijn)*
*  [BUGFIX][DDONS-338] removed double a/ b/ *(martijn)*
*  [FEATURE][DLTS-663] added updated patch for the 219 but for magento 2.4.5-p1 *(Thomas Mondeel)*


## 3.444.1 (2023-02-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.444.1)

*  [BUGFIX][NZA-389] - Fix patch by adding prefixes *(Ruben Panis)*


## 3.444.0 (2023-02-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.444.0)

*  [BUGFIX][NZA-389] - Fix blank screen layout for reset forgotten password *(Ruben Panis)*


## 3.443.0 (2023-02-03)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.443.0)

*  [DOCS] Updated the CHANGELOG.md *(Jeffrey Leeuw)*
*  [FEATURE][HDVS-88] Removed patch, because it is not compatible in all cases. *(Jeffrey Leeuw)*
*  0349_fix_twofactor_2fa_blankscreen_layout_2.4.3_CE_COMPOSER.patch [M2FIX-2] *(Pascal van Gelder)*
*  [FEATURE][PPC-988] Patch to fix saving empty values for customer custom attributes *(Derrick Heesbeen)*
*  [FEATURE][PPC-988] REname patch file *(Derrick Heesbeen)*
*  [FEATURE][PPC-988] Patch to fix saving empty values for customer custom attributes *(Derrick Heesbeen)*
*  [FEATURE][DDONS-239] added patch tot fix 404 when category image is missing *(Dylan Maurits)*


## 3.348.1 (2023-01-13)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.348.1)

*  [FEATURE][HDVS-88] creditmemo bug *(Jeffrey Leeuw)*


## 3.348.0 (2023-01-12)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.348.0)

*  [FEATURE][SWS-154] fix Deprecated error str_replace in tinxit b2bpricing-magento2 *(Justin Demmers)*


## 3.347.0 (2023-01-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.347.0)

*  [BUGFIX][NZA-298] - Change return type to prevent error *(Ruben Panis)*
*  [FEATURE][PPC-968] Fixed indexer_update_all_views not working on 2.4.5-p1 *(Quinn Stadens)*
*  [FEATURE][BOEK-1097] updated patches to be compatible with magento 2.4.5-p1 - patch 0203 is replaced with 0440 - patch 0243 is replaced with 0439 *(Thomas Mondeel)*


## 3.346.1 (2023-01-03)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.346.1)

*  [FEATURE][DLTS-480] added updated patches compatible with 2.4.5-p1. - 436 replaces 198 - 435 replaces 245 - 437 replaces 332 *(Thomas Mondeel)*
*  [BUGFIX][DLTS-480] removed previus wrong duplicated patches *(Thomas Mondeel)*


## 3.346.0 (2023-01-03)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.346.0)

*  [FEATURE][SONPU-587] - Fix blank screen for two factor auth (2fa) when configuring *(Ruben Panis)*
*  [BUGFIX][PPC-907] Added patch to fix experius elasticsearch templates *(Derrick Heesbeen)*
*  [BUGFIX][PPC-907] Added patch to fix experius elasticsearch templates *(Derrick Heesbeen)*
*  [UPGRADE][SSMED-868] Added patch to fix deprication error in adminhtml template from Dart/ProductKeys module. *(Luuk Meijer)*
*  [BUGFIX][NZA-298] - Fix login bug when no wishlist in present for graphql *(Ruben Panis)*
*  [BUGFIX][NZA-298] - Add patch content *(Ruben Panis)*
*  [FEATURE][DLTS-480] added updated patches compatible with 2.4.5-p1. - 352 replaces 198 - 351 replaces 245 - 353 replaces 332 *(Thomas Mondeel)*


## 3.345.0 (2022-11-23)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.345.0)

*  [FEATURE][SGO-449] *(Jeffrey Leeuw)*


## 3.344.0 (2022-11-16)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.344.0)

*  [FEATURE][DSORG-1154] Added patch to fix GA not working with php 2.4.5 *(Quinn Stadens)*


## 3.343.0 (2022-11-15)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.343.0)

*  [FEATURE][DSORG-1111] dotdigital fix plugin invalid filter attribute *(Wouter Langerak)*
*  [REFACTOR][FEATURE/DSORG-1111] renamed patchnumber *(Wouter Langerak)*


## 3.342.0 (2022-11-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.342.0)

*  [FEATURE][SWS-151] fix for empty catalog page *(Dylan Maurits)*


## 3.341.0 (2022-11-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.341.0)

*  [FEATURE][UNIS-5] Add patch to remove graphql dep from fastly 1.2.190 *(Martijn)*
*  [FEATURE][SWS-151] Created patch to fix elasticsearch issue caused by shopby *(Dylan Maurits)*


## 3.339.0 (2022-10-26)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.339.0)

*  [BUGFIX][ADUK-43] Invalid db_schema in sapient/worldpay *(Jeffrey Leeuw)*
*  [FEATURE][PPC-854] Added patch to fix email having hash on top *(Quinn Stadens)*


## 3.337.0 (2022-10-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.337.0)

*  [FEATURE][OA-25] Added security patch *(Quinn Stadens)*


## 3.336.0 (2022-10-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.336.0)

*  [FEATURE][OA-25] Added security patch *(Quinn Stadens)*


## 3.335.0 (2022-10-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.335.0)

*  [FEATURE][OA-25] Added security patch *(Quinn Stadens)*


## 3.334.0 (2022-09-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.334.0)

*  [FEATURE][SBULT-759] add fix filter read more patch *(Justin Demmers)*
*  [DOCS] Updated the CHANGELOG.md *(Justin Demmers)*
*  [FEATURE][SBULT-759] rename 0334 filter fix *(Justin Demmers)*


## 3.333.0 (2022-09-22)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.333.0)

*  [BUGFIX][SDP-920] - Add fix for partial creditmemo subtotal calculation *(Ruben Panis)*
*  [FEATURE][DLTS-467] Patch for correct response code graphql *(Derrick Heesbeen)*
*  [BUGFIX][FEATURE][SFIT-52] 0333_graphql_name_must_be_string_245_CE_COMPOSER.patch *(Jeffrey Leeuw)*
*  [BUGFIX][SFIT-52] Adding patch 0333 *(Jeffrey Leeuw)*
*  [DOCS] Updated the CHANGELOG.md *(Jeffrey Leeuw)*


## 3.331.1 (2022-09-22)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.331.1)

*  [BUGFIX][SDP-920] - Add fix for partial creditmemo subtotal calculation *(Ruben Panis)*
*  [BUGFIX][FEATURE][SFIT-52] 0333_graphql_name_must_be_string_245_CE_COMPOSER.patch *(Jeffrey Leeuw)*


## 3.331.0 (2022-09-12)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.331.0)

*  [BUGFIX][SDP-920] - Add fix for partial creditmemo subtotal calculation *(Ruben Panis)*


## 3.330.0 (2022-08-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.330.0)



## 3.330 (2022-08-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.330)

*  [FEATURE][SFIT-52] see https://github.com/magento/magento2/issues/35918 *(Jeffrey Leeuw)*
*  [DOCS] Updated the CHANGELOG.md *(Jeffrey Leeuw)*
*  [FEATURE][SFIT-52]Wrong file added. *(Jeffrey Leeuw)*
*  [FEATURE][SFIT-52] see https://github.com/magento/magento2/issues/35918 *(Jeffrey Leeuw)*


## 3.329.0 (2022-08-25)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.329.0)

*  [BUGFIX][SDP-867] - Fix cross border tax calculation in adminhtml *(Ruben Panis)*


## 3.328.0 (2022-08-16)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.328.0)

*  0326_backend_collapsible_js_error.patch created online with Bitbucket *(Matthijs Breed)*
*  [BUGFIX][DSOLIS-444] Fix filename *(Matthijs Breed)*
*  [BUGFIX][DSOLIS-443]  Fix vimeo wrapper *(Matthijs Breed)*
*  [FEATURE][RM-660] added patch to fix order address prefix *(Dylan Maurits)*


## 3.325.0 (2022-08-01)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.325.0)

*  [FEATURE][SMSE-302] Add elasticsuite patch *(Martijn van den Kerkhof)*
*  [FEATURE][DLTS-411] added patch to fix gift card amounts not working on custom input *(Quinn Stadens)*


## 3.323.2 (2022-07-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.323.2)

*  [BUGFIX][SGG-535] Fixed patch by using the correct original file structure *(Quinn Stadens)*


## 3.323.1 (2022-07-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.323.1)

*  [BUGFIX][SGG-535] Fixed patch by removing .orig *(Quinn Stadens)*


## 3.323.0 (2022-07-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.323.0)

*  [FEATURE][SSMED-814] Added patch 317 (MC-43048) *(Yannick van de Peppel)*
*  [DOCS] Updated the CHANGELOG.md *(Yannick van de Peppel)*
*  [FEATURE][SBAS-1723] added patch for graphql storeconfig cache magento 2.4.4 *(René Schep)*
*  [FEATURE][SBAS-1723] Added GraphQL stitching optimalization for 2.4.4 *(René Schep)*
*  [FEATURE][SBAS-1723] Added patch for ucfirst failing in pricing render adjusment *(René Schep)*
*  0321_undefined_array_key_frontend_in_cache_frontend_pool_2.4.4_CE_COMPOSER.patch created online with Bitbucket *(René Schep)*
*  [FEATURE][SMSE-302] Add existing patch to work with smile/elasticsuite 2.10.10 *(Martijn van den Kerkhof)*
*  [FEATURE][SGG-535] Made patch to fix broken anowave ec check on order place *(Quinn Stadens)*


## 3.316.0 (2022-07-20)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.316.0)

*  [BUGFIX][DLTS-388] Added patch to fix indexer_update_all_views crashion on exeption with ambiguous entity_id *(Quinn Stadens)*


## 3.315.0 (2022-07-18)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.315.0)

*  [BUGFIX][DLTS-382] Added patch to fix category save not working due to duplicate url rewrites *(Quinn Stadens)*


## 3.314.0 (2022-07-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.314.0)

*  [DOCS] Updated the CHANGELOG.md *(Wouter Langerak)*
*  [BUGFIX][SFTG-437] - Fix deprecated null parameter stripos price permissions 2.4.4 PHP 8.1 *(Ruben Panis)*


## 3.313.0 (2022-07-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.313.0)

*  [FEATURE][DSORG-1020] patch MDVA 43443 part 2 framework *(Wouter Langerak)*


## 3.312.0 (2022-07-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.312.0)

*  [FEATURE][DSORG-1020] patch MDVA-43443 part 1 module email *(Wouter Langerak)*


## 3.311.0 (2022-07-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.311.0)

*  [FEATURE][DSORG-1020] patch MDVA-43395 part 1 module-email *(Wouter Langerak)*


## 3.310.0 (2022-06-27)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.310.0)

*  [BUGFIX][SP-646] Fix config scoping for gift message settings on order repository *(Matthijs Breed)*


## 3.309.0 (2022-06-15)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.309.0)

*  [FEATURE][DDIJK-424] Added patch to fix blacklisting in webforms module *(Quinn Stadens)*


## 3.308.0 (2022-06-09)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.308.0)

*  [FEATURE][SFTG-312] magento/module-shared-catalog error for Magento 2.4.4 on loading the adminhtml "general" tab. *(Boris van Katwijk)*


## 3.307.0 (2022-06-01)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.307.0)

*  [FEATURE][SCSUP-1497] Added patch for webforms 3 to fix migration command *(Quinn Stadens)*


## 3.306.0 (2022-05-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.306.0)

*  [BUGFIX][DLTS-146] Hotfix matrix rate POSTNL changed sort order module version 1.12.1 *(Raymon de Gast)*


## 2.304.1 (2022-05-27)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.304.1)

*  [FEATURE][GSS-789] patch number was already taken *(Dylan Maurits)*


## 2.304.0 (2022-05-27)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.304.0)

*  [FEATURE][GSS-789] added patch to fix channable bug *(Dylan Maurits)*


## 2.303.0 (2022-05-20)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.303.0)

*  [BUGFIX][KS-301] Added patches for MDVA-43395 https://helpx.adobe.com/security/products/magento/apsb22-13.html *(Matthijs Breed)*
*  [feature][SFTG-216] Filesytem deprecated php empty path to empty path string *(stefan.vanechtelt)*


## 3.304.0 (2022-05-04)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.304.0)

*  [FEATURE][GWSM2-1070] - Create patch to fix difference in search results elasticsuite *(rensieeee)*
*  [DOCS] Updated the CHANGELOG.md *(rensieeee)*
*  [BUGFIX][KS-301] Added patches for MDVA-43395 https://helpx.adobe.com/security/products/magento/apsb22-13.html *(Matthijs Breed)*


## 3.301.0 (2022-04-19)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.301.0)

*  [FEATURE][VKPW-161] MDVA-37288_2.4.2 patch's magento/module-quote changes. *(Boris van Katwijk)*


## 3.300.0 (2022-04-13)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.300.0)

*  [FEATURE][UNIS-11] Add patch 0295 *(Martijn van den Kerkhof)*
*  [BUGFIX][GWSM2-985] Fix sorting on virtual categories in elastic suite *(Matthijs Breed)*
*  [FEATURE][GWSM2-1057] - Fixes incorrect search results because of partial search term *(rensieeee)*
*  [BUGFIX][SLFE-141] Patch to fix error for undefined getLinkField() method when loading bundled products *(Yannick van de Peppel)*
*  [DOCS] Updated the CHANGELOG.md *(Yannick van de Peppel)*
*  [FEATURE][SP-586] Amasty Gift Card fix for correct location in amasty checkout *(Luuk Meijer)*


## 3.294.0 (2022-03-31)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.294.0)

*  [FEATURE][GWSM2-977] - Fixed customer account redirecting to 404 on login *(rensieeee)*
*  [DOCS] Updated the CHANGELOG.md *(rensieeee)*
*  [FEATURE][DDONS-106] created patch for integration token activation *(Dylan Maurits)*
*  [FEATURE][DDONS-106] renamed file *(Dylan Maurits)*


## 3.292.0 (2022-03-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.292.0)

*  [BUGFIX][GWSM2-14] Add patch to fix update current_href on afterUpdateLayer *(Jordy Pouw)*


## 3.291.0 (2022-03-24)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.291.0)

*  [BUGFIX][GWSM2-14] Add patch Smile ElasticSuite optimizer undefined searchcontainer js error *(Jordy Pouw)*


## 3.290.0 (2022-03-23)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.290.0)

*  [FEATURE][NIJS-67] Add patch *(Martijn van den Kerkhof)*
*  [BUGFIX][GWSM2-14] Add patch for Smile ElasticSuite limiter change event select *(Jordy Pouw)*


## 3.287.1 (2022-03-14)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.287.1)

*  [BUGFIX][STEVU-561] Add second part of patch *(Matthijs Breed)*


## 3.287.0 (2022-03-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.287.0)

*  [BUGFIX][BOEK-731] Replacement for 255 2.4.2 CE *(FabianBank)*
*  [DOCS] Updated the CHANGELOG.md *(FabianBank)*
*  [BUGFIX][BOEK-731] Rename .PATCH to .patch *(FabianBank)*
*  [BUGFIX][BOEK-731] Renamed .PATCH to .patch *(FabianBank)*
*  [BUGFIX][BOEK-731] Revert file to original content *(FabianBank)*
*  [BUGFIX][STEVU-561] patch 0287, strtotime error on null when admin session expires *(Matthijs Breed)*


## 3.286.0 (2022-03-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.286.0)

*  [BUGFIX][GWSM2-749] Fix aheadworks blog MagentoInventory dependency *(Jordy Pouw)*


## 3.284.0 (2022-03-09)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.284.0)

*  [FEATURE][OA-22] Added patch to fix dotmailer accessing a template in magento sendfriend module while module is in replace *(Quinn Stadens)*
*  [DOCS] Updated the CHANGELOG.md *(Quinn Stadens)*
*  [UPGRADE][RM-535] - Added patches for phpcs and phpmd scans. Allows connector to detect bitbucket pipelines to disable itself *(Rens Wolters)*
*  [FEATURE][IBS-290] - Fixed addition configurable product pricing errors *(rensieeee)*
*  [FEATURE][IBS-290] - Moved patch to patches folder *(rensieeee)*
*  [FEATURE][DSORG-843] Added patch for customer with multiple newsletter subscriptions *(Quinn Stadens)*
*  [FEATURE][DSORG-843] Fixed typo in patch name *(Quinn Stadens)*
*  [BUGFIX][DSORG-843] Fixed patch by removing .orig in the file *(Quinn Stadens)*
*  [BUGFIX][NIJH-339] Add patch: Fix pagebuilder editor breaking on load when unused stage styling exists *(Rik Bandsma)*
*  [DOCS] Updated the CHANGELOG.md *(Rik Bandsma)*
*  README.md edited online with Bitbucket *(Dylan Maurits)*
*  deprecated.json edited online with Bitbucket *(Dylan Maurits)*
*  deprecated.json edited online with Bitbucket *(Dylan Maurits)*
*  [BUGFIX][IBS-340] Added new version for patch 260 with out of stock product fix *(Lewis Voncken)*
*  [BUGFIX][NIJH-368] Add tig postnl patch that fixes checkout breaking error *(Rik Bandsma)*
*  [BUGFIX][NIJH-368] Fix 264 by reversing patch diff *(Rik Bandsma)*
*  [BUGFIX][PWAI-452] Add patch to fix strtotime exception in adminsessioninfo. See https://github.com/magento/magento2/pull/34514 *(Martijn van den Kerkhof)*
*  [FEATURE][BACI-1] - Added MDVA-43395 patches loosely coupled to patches *(rensieeee)*
*  [FEATURE][MDVA-43443] - Added patches *(rensieeee)*
*  [FEATURE][MDVA-43443] - Add patch *(rensieeee)*
*  [FEATURE][MDVA-43343] - added 2.3.6-p1 patch *(rensieeee)*
*  [FEATURE][MDVA] - Added final patch bundles. Work for all versions. Include magetrend hotfix *(rensieeee)*
*  [FEATURE][PWAI-344] Add patch to add 209 status code for graphql *(Martijn van den Kerkhof)*
*  [FEATURE][PWAI-344] Rename patch *(Martijn van den Kerkhof)*
*  [FEATURE][PWAI-344] Update patch *(Martijn van den Kerkhof)*
*  [BUGFIX] Remove unnecessary patch *(Martijn van den Kerkhof)*
*  [BUGFIX] Add preg_replace to return *(Martijn van den Kerkhof)*
*  [FAETURE][IBS-438] - Added patch to always display payment method in order history *(rensieeee)*
*  [BUGFIX][NIJH-426] Add Akeneo patch *(Martijn van den Kerkhof)*
*  [BUGFIX][NIJH-426] Fix file path *(Martijn van den Kerkhof)*
*  [feature][NAU-971] split patches 0035 into 0277, 0278; and 0104 into 0279, 0280. *(Wouter Langerak)*
*  Updated README with new docs for composer.patches.json *(Rens Wolters)*
*  [BUGFIX][SDP-758] - Add patch for fixing sitemap generation if image title is empty *(Ruben Panis)*
*  [BUGFIX][SDP-839] - Patch MC-42332 to fix invoice/creditmemo/order loading in adminhtml *(Ruben Panis)*
*  [FEATURE][PWAI-448] Added patch for fast url resolver, backport based on 2.4.3 *(Lewis Voncken)*
*  [FEATURE][GWSM2-774] Add patch for Smile ElasticSuite category filter link click fix *(Jordy Pouw)*


## 3.256.0 (2021-12-23)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.256.0)

*  Patch no. 47 does not exist. Because of this. Indexing of the patches website does not work. Adds patch 47 with a placeholder patch *(rensieeee)*
*  Updated README for new patches.experius.nl info *(Rens Wolters)*
*  [FEATURE][BACI-485] - Added still applicable testsuite patches to patches collection *(rensieeee)*
*  [FEATURE][BACI-485] - Removed placeholder patch, named patch 47 with .patch extension *(rensieeee)*
*  [FEATURE][SCGE-217] Patch added to fix wrong url rewrite on magento 2.4.2 *(Quinn Stadens)*
*  [FEATURE][IBS-269] Add patch to fix graphql configurableproduct price without children *(Jordy Pouw)*


## 3.252.0 (2021-12-13)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.252.0)

*  [BUGFIX][ATSS-97] Scope error when adding configurable product to cart on non default store Based on https://github.com/magento/magento2/pull/31736 *(Matthijs Breed)*


## 3.251.0 (2021-12-13)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.251.0)

*  [FEATURE] Add patches related to Smile Elasticsuite *(Martijn van den Kerkhof)*


## 3.249.0 (2021-12-13)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.249.0)

*  [BUGFIX[DRRS-111] Fix typo in patch 237 StringD! -> String! *(Jordy Pouw)*
*  Add patch 199 deprated list 2.4.2 *(Jordy Pouw)*
*  Updated README: Description of applying app/code patches. Included applying of patches.experius.nl *(Rens Wolters)*
*  Added Patcher 2 => 3 upgrade steps to Readme *(Rens Wolters)*
*  [BUGFIX][SBWT-36] - Fix invoice items in email when invoice is created via API *(Ruben Panis)*


## 3.248.0 (2021-12-03)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.248.0)

*  deprecated.json edited online with Bitbucket *(Ruben Panis)*
*  [TASK][SDP-758] - Add 2.4.3 version for patch 225 *(Ruben Panis)*


## 3.247.0 (2021-12-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.247.0)

*  [BUGFIX][EMGA-350] Fix call to member function on array in magento checkout when extension attributes are set *(Matthijs Breed)*


## 3.243.3 (2021-11-26)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.243.3)

*  0246_hotfix_anowave_pvc_with_tinxit_b2b_pricing_performance_CE_2.3.6_COMPOSER.patch created online with Bitbucket *(Jasper van den Berg)*


## 3.243.2 (2021-11-25)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.243.2)

*  [FEATURE][PWAI-403] Add patches from experius coregraphql *(Matthijs Breed)*


## 3.243.1 (2021-11-25)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.243.1)

*  README.md edited online with Bitbucket *(Rens Wolters)*
*  [FEATURE][BACI-485] - Re-added deprecated.json for insight in deprecation until there is an alternative *(Rens Wolters)*
*  [DOCS] Updated the CHANGELOG.md *(Rens Wolters)*
*  [BUGFIX][PWAI-403] Fix patch 221 *(Matthijs Breed)*


## 3.243.0 (2021-11-12)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/3.243.0)

*  [FEATURE][BACI-485] - Removed entire module structure. Included magento/quality-patches and cweagans/composer-patches in composer.json *(Rens Wolters)*


## 2.243.0 (2021-11-10)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.243.0)

*  [FEATURE][BOEK-631] Added patch for visibility search only support in graphql *(Lewis Voncken)*


## 2.242.0 (2021-11-09)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.242.0)

*  [BUGFIX][WBT-293] Add patch to fix Amasty spinner on product category *(Pierre Denissen)*


## 2.241.0 (2021-11-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.241.0)

*  [FEATURE][BOEK-641] Added patch for correct free shipping incombination with cart price rules + TableRate Shipping *(Lewis Voncken)*


## 2.240.0 (2021-11-04)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.240.0)

*  [FEATURE][SBULT-435] - Fixes source model assigning on multiselect attribute creation *(Rens Wolters)*


## 2.239.2 (2021-11-04)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.239.2)

*  [FEATURE][BACI-540] Add check for existence of file *(Martijn van den Kerkhof)*


## 2.239.1 (2021-11-04)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.239.1)

*  [FEATURE][BACI-511] Redirect stderr to stdout to catch additional errors *(Martijn van den Kerkhof)*


## 2.239.0 (2021-11-03)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.239.0)

*  [BUGFIX][LISL-220] Add patch 239 to fix myparcelnl msi dependencies *(Matthijs Breed)*


## 2.238.0 (2021-11-01)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.238.0)

*  [FEATURE][SBIO-679] Rewrote patch 0077 for amasty/base 1.12.5 *(Yannick van de Peppel)*


## 2.237.0 (2021-10-27)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.237.0)

*  [FAETURE][IBS-173] - Added graphql sales id fix *(Rens Wolters)*
*  [FEATURE][IBS-173] - Renamed patch 236 to 237 *(Rens Wolters)*


## 2.236.0 (2021-10-27)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.236.0)

*  [FEATURE][DSOLIS-277] Added patch to fix catalog output helper compare list issue *(Jordy Pouw)*


## 2.235.1 (2021-10-19)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.235.1)

*  [BUGFIX][VBK-1105] fixed issue with patch not being applied due to wrong diff headers *(Thomas Mondeel)*


## 2.235.0 (2021-10-13)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.235.0)

*  [FEATURE][PPC-446] Patch added to fix error on nonexistant amasty css file *(Quinn Stadens)*


## 2.234.0 (2021-10-12)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.234.0)

*  [FEATURE][DRRS-106] Added GraphQl patch to fix special prices not respecting from and to dates *(René Schep)*


## 2.233.2 (2021-10-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.233.2)

*  [BUGFIX][NIJH-188] Update patch 229 for correct magento version *(Rik Bandsma)*


## 2.233.1 (2021-10-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.233.1)

*  [FEATURE][SFIT-33] removed patch duplicate *(Dylan Maurits)*
*  [DOCS] Updated the CHANGELOG.md *(Dylan Maurits)*
*  [BUGFIX][NIJH-188] Fix triple function declaration *(Rik Bandsma)*


## 2.233.0 (2021-10-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.233.0)

*  [BUGFIX][DSOLIS-260] Wrote patch for problem described in DSOLIS-260 (VAT being calculated over shipping threshold) *(EXP0025)*
*  [BUGFIX][DSOLIS-260] Correction (was 2 lines being added, is now 2 lines being removed) *(EXP0025)*
*  [BUGFIX][DSOLIS-260] Changed patch number *(EXP0025)*


## 2.232.2 (2021-10-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.232.2)

*  [BUGFIX][NIJH-223] Updated patch 0232 *(Martijn van den Kerkhof)*


## 2.232.1 (2021-10-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.232.1)

*  [DOCS] Updated the CHANGELOG.md *(Martijn van den Kerkhof)*
*  [BUGFIX][NIJH-223] Fix patch path *(Martijn van den Kerkhof)*


## 2.232.0 (2021-10-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.232.0)

*  [BUGFIX][NIJH-223] Add patch to fix 'invalid security or form key' message upon admin login in 2.4.3 CE *(Martijn van den Kerkhof)*


## 2.231.1 (2021-10-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.231.1)

*  [FEATURE][SFIT-33] changed deprecated.json *(Dylan Maurits)*


## 2.231.0 (2021-10-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.231.0)

*  [FEATURE][SFIT-33] added patch 229 to fix negative backorder threshold *(Dylan Maurits)*
*  [FEATURE][SFIT-33] added to deprecated.json *(Dylan Maurits)*
*  [FEATURE][SFIT-33] indentation *(Dylan Maurits)*
*  [FEATURE][SFIT-33] changed patch number *(Dylan Maurits)*


## 2.230.1 (2021-10-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.230.1)

*  [BUGFIX][ATSS-37] Reworked patch 221 that wasn't applying correctly *(Matthijs Breed)*


## 2.230.0 (2021-10-05)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.230.0)

*  [FEATURE][BACI-508] Use latest version for 'magento/magento-coding-standard' *(Lewis Voncken)*
*  [FEATURE][DSORG-756] Added patch for dotmailer to fix pipeline deployment when database changes are made *(Quinn Stadens)*


## 2.229.0 (2021-10-01)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.229.0)

*  [BUGFIX][NIJH-188] Fix breaking JS in Magento_Customer//customer-data.js. Based on https://github.com/magento/magento2/pull/31940 *(Rik Bandsma)*


## 2.228.0 (2021-09-23)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.228.0)

*  [FEATURE][NAU-787] Added backport of redis parallel cache generation from 2.4 *(Egor Dmitriev)*


## 2.227.0 (2021-09-21)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.227.0)

*  [FEATURE][PPC-508] Added patch for amasty sorting module to fix breaking on no small image *(Quinn Stadens)*


## 2.226.0 (2021-09-21)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.226.0)

*  [FEATURE][SGG-434] added patch to disable connector option value export failsave that caused incorrect matching when a value can be a value and can be a option id *(Thomas Mondeel)*


## 2.225.1 (2021-09-20)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.225.1)

*  [BUGFIX][SDP-743] - Fix invoice total calculation with discount *(Ruben Panis)*


## 2.225.0 (2021-09-16)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.225.0)

*  [FEATURE][BACI-135] Update Bitbucket Pipelines with magento/magento-coding-standard fixed version 8 *(Experius)*
*  [BUGFIX][SDP-743] - Fix total invoiced when cart rule discount applied *(Ruben Panis)*


## 2.224.1 (2021-09-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.224.1)

*  [FEATURE] Fixed 0223 stitching patch to actually apply fully *(René Schep)*


## 2.224.0 (2021-09-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.224.0)

*  [FEATURE] Added schema stitching optimisation patches *(René Schep)*


## 2.222.0 (2021-09-03)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.222.0)

*  deprecated.json edited online with Bitbucket *(Matthijs Breed)*
*  [BUGFIX][DSORG-673] Added patch to fix issue where only first customer info is shown in backend grid. *(Egor Dmitriev)*


## 2.221.1 (2021-08-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.221.1)

*  [BUGFIX][SBN-184] Fix malformed patch 220 *(Matthijs Breed)*


## 2.221.0 (2021-08-23)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.221.0)

*  [FEATURE][ATSU-147] Added support for graphql cart error codes 2.4.2 *(René Schep)*


## 2.220.0 (2021-08-17)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.220.0)

*  [BUGFIX][SUBI-271] Added patch for customer data undefined storage error *(Bodhi Mooij)*


## 2.219.0 (2021-08-16)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.219.0)

*  [BUGFIX][DLTS-94] Added patch to hide products in search, after filtering, that have visibility catalog only *(Derrick Heesbeen)*


## 2.218.1 (2021-08-12)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.218.1)

*  [BUGFIX][ATSU-131] Corrected malformed patch 217 *(Matthijs Breed)*


## 2.218.0 (2021-08-10)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.218.0)

*  [BUGFIX][SUBI-193] - Added patch to fix incorrect script loading. Script was loaded self closing which is not allowed in Magento 2.4. *(Ton Matton)*


## 2.217.0 (2021-08-09)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.217.0)

*  [BUGFIX][ATSU-131] Bundle product price fix 2.4 *(matthijs-breed-experius)*


## 2.215.0 (2021-08-09)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.215.0)

*  [BUGFIX][SPC-2934] - Fixes all products being rendered on category page, even when pagination is configurred correctly *(Rens Wolters)*
*  0215_fix_category_page_pagination_2.3.6-p1_CE_COMPOSER.patch edited online with Bitbucket *(Rens Wolters)*
*  [BUGFIX][SPC-2934] -Added functionality for request params: current page, page limit, sort attribute, sort direction *(Rens Wolters)*
*  [BUGFIX][SPC-2934] - Added support for customer defined list mode *(Rens Wolters)*


## 2.216.1 (2021-08-09)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.216.1)

*  [BUGFIX] - Added correct file path *(Ton Matton)*


## 2.216.0 (2021-08-09)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.216.0)

*  [FEATURE][SUBI-193] - Added patch to fix category pagination for Magetno 2.4.2 *(Ton Matton)*


## 2.214.1 (2021-08-03)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.214.1)

*  [DOCS][SPC-2934] Update deprecated.json for patches 67, 78, 84, 108, 112 *(Jordy Pouw)*
*  [BUGFIX][DLTS-69] Updated patch 199 to prevent unnecessary error on login *(Lewis Voncken)*


## 2.214.0 (2021-07-28)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.214.0)

*  [FEATURE][SCSUP-1296] Added patch to set product alert stock id in email template after chekcing for customer to prevent sending from the wrong store *(Quinn Stadens)*


## 2.213.0 (2021-07-21)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.213.0)

*  [TASK][SFTG-317] - Add collecttotals to check for shipping method to prevent "Carrier with such method not found" error in checkout *(Ruben Panis)*


## 2.212.0 (2021-07-21)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.212.0)

*  [BUGFIX][DLTS-7] updated inline if in patch 0201 *(Lewis Voncken)*
*  [FEATURE][SLFE-173] Solved tax issue with cart rules *(Dylan Maurits)*
*  [FEATURE][SLFE-173] refactor *(Dylan Maurits)*


## 2.210.4 (2021-07-01)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.210.4)

*  [BUGFIX][DRRS-21] Solved invalid previous error *(Lewis Voncken)*


## 2.210.3 (2021-07-01)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.210.3)

*  [BUGFIX][DGCC-175] Updated patch 0123 (force config reload after save) for Magento 2.3.6 *(EXP0025)*


## 2.210.2 (2021-07-01)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.210.2)

*  0209_deletes_product_image_on_product_delete_2.4.2_CE_COMPOSER.patch edited online with Bitbucket *(Dylan Maurits)*
*  0209_deletes_product_image_on_product_delete_2.4.2_CE_COMPOSER.patch edited online with Bitbucket *(Dylan Maurits)*


## 2.210.1 (2021-06-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.210.1)

*  [BUGFIX][DLTSM2-684] Corrected patch 0181 Tierprices Tax *(Derrick Heesbeen)*


## 2.210.0 (2021-06-28)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.210.0)

*  [FEATURE][OA-3] Patch added to fix checkout js in magento 2.4.2 *(Quinn Stadens)*


## 2.209.0 (2021-06-24)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.209.0)

*  [FEATURE][FLM-219] Added patch to delete product image on product delete *(Dylan Maurits)*


## 2.208.0 (2021-06-16)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.208.0)

*  [FEATURE][DLTSM2-496] Added patch to allow grouped products in search when product has no child products *(Lewis Voncken)*


## 2.207.1 (2021-06-09)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.207.1)

*  [BUGFIX][DLTSM2-590] Return 209 in GraphQl when user is logged out and guest cart should be created *(Lewis Voncken)*


## 2.207.0 (2021-06-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.207.0)

*  [FEATURE][DLTSM2-533] Added patch so the giftCard amount on the quote address is correct *(Lewis Voncken)*


## 2.206.0 (2021-06-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.206.0)

*  [FEATURE][DLTSM2-590] Solved error when merging persistent customer cart in graphql *(Lewis Voncken)*


## 2.205.0 (2021-06-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.205.0)

*  [FEATURE][DSORG-609] Added patch to fix setting compare on no visitor id after using login as customer *(Quinn Stadens)*


## 2.204.2 (2021-06-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.204.2)

*  [BUGFIX][BOEK-125] moved patch 0203 to the patch folder *(Lewis Voncken)*


## 2.204.1 (2021-06-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.204.1)

*  [BUGFIX][DOR-312] Renamed patch to CE instead of EE and removed .original from patch *(Quinn Stadens)*
*  [BUGFIX][DOR-312] added /vendor to patch to make it patch succesfully *(Quinn Stadens)*


## 2.204.0 (2021-06-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.204.0)

*  [BUGFIX][DOR-312] Patch added to fix configurator products not showing in category when no price is set incorrectly *(Quinn Stadens)*


## 2.203.0 (2021-06-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.203.0)

*  [BUGFIX][BOEK-125] Added patch to resolve GraphQl Schema error 'Error: OrderItemInterface.id expects type "ID!" but OrderItem.id provides type "String!".' *(Lewis Voncken)*


## 2.202.0 (2021-05-26)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.202.0)

*  [FEATURE][DLTSM2-610] Show shipping price including tax for graphql *(Lewis Voncken)*


## 2.201.1 (2021-05-26)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.201.1)

*  [BUGFIX][DLTSM2-596] Solved invalid path by replacing magento to klarna *(Lewis Voncken)*


## 2.201.0 (2021-05-26)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.201.0)

*  [FEATURE][DLTSM2-586] Added patch for klarnaGraphQl, multiplsestoreview quote support and add graphql status code *(Lewis Voncken)*


## 2.198.1 (2021-05-20)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.198.1)

*  [BUGFIX][BACI-166] Updated invalid country_id in the allow_save_in_address_book_for_new_address in graphql *(Lewis Voncken)*


## 2.198.0 (2021-05-19)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.198.0)

*  [FEATURE][DLTSM2-512] Added patch to prevent incorrect grandtotal when applying coupon code through graphql *(Lewis Voncken)*


## 2.197.0 (2021-05-19)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.197.0)

*  [FEATURE][BACI-135] Added bitbucket-pipelines.yml with Static Code Scan *(Lewis Voncken)*
*  [FEATURE][DLTSM2-582] Solved data fetch error on category page which includes grouped product which have no associated products *(Lewis Voncken)*


## 2.196.0 (2021-05-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.196.0)

*  [FEATURE][SGG-377] Added patch 0196 that fixes an error when adding categories to cart price rules via click. *(Gijs Blanken)*


## 2.195.0 (2021-05-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.195.0)

*  [BUGFIX][SDP-687] - Fixed critical error: The following tag(s) are not allowed: img *(Ruben Panis)*
*  [TASK][SDP-682] - Add new patch to deprecated *(Ruben Panis)*


## 2.194.0 (2021-05-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.194.0)

*  [TASK][DSORG-620] Added patch for dotmailer which breaks the api due to incorrect typing *(Egor Dmitriev)*


## 2.193.0 (2021-05-05)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.193.0)

*  [BUGFIX][DLTSM2-561] Hotfix matrix rate POSTNL changed sort order *(Derrick Heesbeen)*


## 2.192.0 (2021-04-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.192.0)

*  [FEATURE][SOLUI-104] added confirm to prevent data loss when the attribute set is saved *(Thomas Mondeel)*


## 2.191.0 (2021-04-20)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.191.0)

*  [FEATURE][FLM2-103] - Added patch that fixes errors during setup:upgrade command in the Amasty_MostViewed module *(TonMatton)*


## 2.190.1 (2021-04-14)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.190.1)

*  [BUGFIX][DLTSM2-507] Added a method_exists call on getPageSize. *(Gijs Blanken)*


## 2.190.0 (2021-04-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.190.0)

*  [FEATURE][SUBI-193] - Added fix for products nog showing in categories in Magento 2.4.2 *(Ton Matton)*


## 2.189.0 (2021-04-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.189.0)

*  [FEATURE][DLA-10] Show configurable tier prices when base price is equal *(Simone Puhl)*


## 2.188.0 (2021-03-26)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.188.0)

*  [FEATURE][SAHH-149] Fix for database issues when multiple filters are select in layered navigation *(René Schep)*


## 2.187.0 (2021-03-26)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.187.0)

*  [FEATURE][PWARS-153] Added simpelere graphql patch to use cart in multiple storeviews *(René Schep)*


## 2.186.1 (2021-03-19)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.186.1)

*  [FEATURE][DLTSM2-416] Added extra fix to existing patch. Pagesize option for related products in graphql call *(Derrick Heesbeen)*


## 2.186.0 (2021-03-17)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.186.0)

*  [BUGFIX][DLTSM2-436] Added patch to only show products visible product in related product graphql call *(Derrick Heesbeen)*


## 2.185.1 (2021-03-15)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.185.1)

*  [BUGFIX][DLTSM2-370] bugfixed patch 181, added special_price to product collection *(Derrick Heesbeen)*


## 2.185.0 (2021-03-15)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.185.0)

*  [BUGFIX][HELV-18] Added patch to fix customer-data JS issue in 2.4.1 *(Matthijs Breed)*


## 2.184.0 (2021-03-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.184.0)

*  [FEATURE][NAU-699] Added patch to solve missing products during visualmerchandiser rebuild because of storeview values *(Lewis Voncken)*


## 2.183.0 (2021-03-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.183.0)

*  [FEATURE][DLTSM2-378] Solve issue with old special price in graphql when fields are not in query *(Lewis Voncken)*


## 2.182.0 (2021-03-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.182.0)

*  [FEATURE][DLTSM2-404] Added patch for additional error handling in klarna graphql *(Lewis Voncken)*


## 2.181.0 (2021-03-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.181.0)

*  [FEATURE][DLTSM2-378] Added patch for GraphQl special_price should include date filter from/to and tierprice website filter *(Lewis Voncken)*


## 2.180.1 (2021-03-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.180.1)

*  [BUGFIX][NAU-700] Updated patch to prevent additional join *(Lewis Voncken)*


## 2.180.0 (2021-03-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.180.0)

*  [FEATURE][BACI-166] Added patch to allow save_inaddress_book for new address in GraphQl *(Lewis Voncken)*


## 2.178.0 (2021-03-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.178.0)

*  [FEATURE][BACI-166] Added patch to ensurel url suffix resolvers return string for graphql schema validation in 2.4.1 *(Lewis Voncken)*


## 2.177.0 (2021-03-01)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.177.0)

*  [PATCH][PM2-334] - Patch to correctly transliterate product urls. This prevents that the url key can contain spaces and replaces them with dashes *(TonMatton)*


## 2.176.0 (2021-02-24)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.176.0)

*  [FEATURE][SOLIS-676] Added patch to prevent exception when customer section name is unknown *(Lewis Voncken)*


## 2.175.0 (2021-02-23)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.175.0)

*  [BUGFIX][SWOU-626] Added patch to fix sendcloud creating an error when saving shipping information *(Matthijs Breed)*


## 2.174.0 (2021-02-22)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.174.0)

*  [FEATURE][NAU-686] Disable stock table join for category filter when display out of stock items is set to true in combination with Amasty_Shopby *(Lewis Voncken)*


## 2.173.0 (2021-02-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.173.0)

*  [FEATURE][NAU-631] Use addError to allow html for errors returned during a login attempt - for example unconfirrmed email *(Lewis Voncken)*


## 2.172.1 (2021-01-26)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.172.1)

*  [FEATURE] Added patch that fixes shipment tracking links *(Dylan Maurits)*
*  [DOCS] Changed documentation on creating patches *(Dylan Maurits)*


## 2.171.1 (2021-01-22)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.171.1)

*  [DOCS] Updated the CHANGELOG.md *(Quinn Stadens)*
*  0171_fix_email_subject_with_special_characters_2.3.5-p2_CE_COMPOSER.patch edited online with Bitbucket *(Quinn Stadens)*


## 2.171.0 (2021-01-22)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.171.0)

*  [FEATURE][PSTR-199] Patch added to fix special characters being sent wrong in email subject *(Quinn Stadens)*


## 2.170.0 (2021-01-21)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.170.0)

*  [FEATURE][DRED-107] Updated deprecated..json patch 0088 *(Jordy Pouw)*
*  0170_fix_updating_REST_product_multiselect_attribute_2.3.5-p2_CE_COMPOSER.patch created online with Bitbucket *(Jasper van den Berg)*


## 2.169.0 (2021-01-19)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.169.0)

*  [FEATURE][VBK-961] added patch for free order without pay base_row_total_incl_tax calculation *(Thomas Mondeel)*


## 2.168.0 (2021-01-19)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.168.0)

*  [FEATURE][SFA-91] 2.3.6 Prevent newsletter unsubscribe email on account creation - Enable successful newsletter subscription after email-confirmation *(Jordy Pouw)*


## 2.167.0 (2021-01-19)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.167.0)

*  [FEATURE][DOR-203] added patch for the general -> general not loading when shared catalog is used *(Thomas Mondeel)*


## 2.166.0 (2021-01-13)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.166.0)

*  [FEATURE][SBULT-241] - Added Webforms Pro patch that switches the way the result grid is loaded from more than 61 fields to more than 9 fields *(TonMatton)*


## 2.165.0 (2021-01-12)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.165.0)

*  [FEATURE][SPC-2404] Patch for cached pricebox zone *(Jordy Pouw)*


## 2.164.0 (2021-01-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.164.0)

*  [FEATURE][NAU-586] Prevent createPost from automatically confirm email address while email confirmation is required *(Lewis Voncken)*


## 2.163.0 (2021-01-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.163.0)

*  [BUGFIX][PPC-173] Added patch 163 to fix session issues caused by amasty finder. This issue causes user to only have one session, either frontend or backend *(Egor Dmitriev)*


## 2.162.0 (2020-12-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.162.0)

*  [FEATURE][SBAS-1387] Added patch so invoice emails do not get send in adminhtml if "Email Copy of Invoice" is not checked *(René Schep)*


## 2.161.3 (2020-12-21)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.161.3)

*  [FEATURE][PWARS-153] Additional fixes *(René Schep)*


## 2.161.2 (2020-12-21)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.161.2)

*  [FEATURE][PWARS-153] Fixed missing "use"s *(René Schep)*


## 2.161.1 (2020-12-21)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.161.1)

*  [FEATURE][PWARS-153] Added 0161 to deprecated.json *(René Schep)*


## 2.161.0 (2020-12-21)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.161.0)

*  [FEATURE][PWARS-153] Added patch to allow cart to not break when switching storeviews within website *(René Schep)*


## 2.160.1 (2020-12-18)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.160.1)

*  [BUGFIX] [SNBB166] Free shipping rule subtotal is inconsistent, add option to specifically specify price incl tax *(Matthijs Breed)*


## 2.160 (2020-12-16)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.160)

*  [FEATURE][DSORG-444] Fixed magento issue where customer dates in admin are not saved properly due to formatting issues. See https://github.com/magento/magento2/issues/27310 *(Egor Dmitriev)*


## 2.159.1 (2020-12-15)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.159.1)

*  [TASK][SFTG-265] - Fix formatting for URL key to filter spaces and more *(Ruben Panis)*
*  0159_include_tax_for_freeshipping_min_subtotal_2.3.5-p2_CE_COMPOSER.patch created online with Bitbucket *(Jasper van den Berg)*
*  0159_include_tax_for_freeshipping_min_subtotal_2.3.5-p2_CE_COMPOSER.patch created online with Bitbucket *(Jasper van den Berg)*


## 2.159.0 (2020-12-15)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.159.0)

*  [TASK][SFTG-265] - Fix formatting for URL key to filter spaces and more *(Ruben Panis)*
*  0159_include_tax_for_freeshipping_min_subtotal_2.3.5-p2_CE_COMPOSER.patch created online with Bitbucket *(Jasper van den Berg)*


## 2.158.0 (2020-12-15)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.158.0)

*  [TASK][SFTG-265] - Fix formatting for URL key to filter spaces and more *(Ruben Panis)*


## 2.157.0 (2020-12-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.157.0)

*  [FEATURE][DRL-612] added resize only requested image 2.3.6 *(Thomas Mondeel)*


## 2.156.0 (2020-12-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.156.0)

*  [FEATURE][DLTSM2-141] GraphQl Price Filter removed range wildcard *(Lewis Voncken)*


## 2.155.0 (2020-11-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.155.0)

*  [FEATURE] [DRL-842] Prevent Exception 'Item (%s) with the same ID "%s" aleady exists. and log to var/log/experius.log *(Lewis Voncken)*


## 2.154.0 (2020-11-24)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.154.0)

*  [FEATURE][SPC-2382] - Fix getBrandAliases method of Amasty Shop By Brand as to properly support multistore brand aliases *(Cas Satter)*
*  [BUGFIX][SPC-1966] - Change patch name as another patch 153 was introduced at the same time *(Cas Satter)*


## 2.153.0 (2020-11-24)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.153.0)

*  [BUGFIX] - Fix correct sitemap reference in robots.txt with multiple stores *(Ruben Panis)*


## 2.152.0 (2020-11-23)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.152.0)

*  [BUGFIX] Subtotal used in cart price rules (Magento 2.3.5) always consider subtotal excl. tax. (Patch 0152_subtotal_excl_tax_is_always_used_for_cart_rules_CE_2.3.5_COMPOSER.patch) *(Boris van Katwijk)*


## 2.151.0 (2020-11-19)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.151.0)

*  [FEATURE] 0151_fix_escaped_image_custom_attributes_2.3.3_COMPOSER.patch *(Jordy Pouw)*


## 2.149.1 (2020-11-17)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.149.1)

*  0150_fix_date_invoice_pdf_2.3.5-p2_CE_COMPOSER.patch created online with Bitbucket *(Jasper van den Berg)*


## 2.149.0 (2020-11-09)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.149.0)

*  [REFACTOR] Renamed patch 148 with type COMPOSER in the name *(Lewis Voncken)*
*  [FEATURE] [SCSUP-1077] Added Patch to prevent the customer group to be reset to the Default Customer Group on Customer Edit in Frontend *(Lewis Voncken)*


## 2.148.0 (2020-10-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.148.0)

*  [FEATURE][DPLANT-609] - Applied Magento patch for fixing shipping cost for tablerate *(Ruben Panis)*


## 2.147.0 (2020-10-27)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.147.0)

*  [FEATURE] [DLTSM2-116] Added compatibility patch for TIG_PostNL and not installed MSI *(Lewis Voncken)*


## 2.146.0 (2020-10-22)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.146.0)

*  0146_optimize_slow_filtering_amasty_shopby_issue_2.3.4-p2_CE_COMPOSER.patch created online with Bitbucket *(Jasper van den Berg)*


## 2.145.3 (2020-10-22)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.145.3)

*  [REFACTOR] [BACI-123] use shell_exec incombination with '// phpcs:ignore Magento2.Security.InsecureFunction' *(Lewis Voncken)*
*  0146_optimize_slow_filtering_amasty_shopby_issue_2.3.4-p2_CE_COMPOSER.patch created online with Bitbucket *(Jasper van den Berg)*
*  [REFACTOR] [BACI-157] Removed setup_version from module.xml *(Lewis Voncken)*
*  [DOCS] Updated the CHANGELOG.md *(Lewis Voncken)*
*  [FEATURE] [BACI-154] Added strict_types=1 and added License *(Lewis Voncken)*


## 2.145.2 (2020-10-20)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.145.2)

*  0146_optimize_slow_filtering_amasty_shopby_issue_2.3.4-p2_CE_COMPOSER.patch created online with Bitbucket *(Jasper van den Berg)*


## 2.145.1 (2020-10-15)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.145.1)

*  [REFACTOR] [BACI-123] solved errors based on php code sniffer *(Lewis Voncken)*


## 2.145.0 (2020-10-15)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.145.0)

*  [FEATURE][DSORG-345] Added patch so only the requested image is resized through the frontend *(Bas van der Louw)*


## 2.144.0 (2020-10-12)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.144.0)

*  [BUGFIX] [SOLIS-302] Added patch to prevent Amasty Layered Navigation from crashing when Magento Elasticsearch is disabled instead of replaced in composer.json due to Cloud restrictions *(Matthijs Breed)*


## 2.143.0 (2020-10-09)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.143.0)

*  0143_best4mage_cptp_tierprice_save_hotfix_2.3.5_CE_COMPOSER.patch created online with Bitbucket *(Matthijs Breed)*


## 2.142.0 (2020-10-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.142.0)

*  [FEATURE][SAHH-112] - Added patch to fix bulk product attribute update via consumers by fixing area code already set error *(Ruben Panis)*


## 2.141.0 (2020-09-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.141.0)



## 2.141 (2020-09-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.141)

*  [BUGFIX][DSORG-20] Added patch that sorts attributes in compare view in the order of the attribute set *(Derrick Heesbeen)*


## 2.140.2 (2020-08-26)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.140.2)

*  [FEATURE] Added 0070 to depricated json *(Daphne Witmer)*


## 2.140.1 (2020-08-25)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.140.1)

*  [FEATURE] Updated depricated.json and renamed patch *(Matthijs Breed)*


## 2.140.0 (2020-08-24)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.140.0)

*  [FEATURE] Added patch to fix html in customer login error session message *(dheesbeen)*


## 2.139.0 (2020-08-19)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.139.0)

*  [FEATURE] [DRL-771] Added patch to prevent 404 for storeswitch for logged in users by setting the sortorder for hashgenerator *(Lewis Voncken)*


## 2.138.3 (2020-08-13)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.138.3)

*  [FEATURE] Added 0129 to deprecated.json *(René Schep)*


## 2.138.2 (2020-08-12)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.138.2)

*  [FEATURE] Add patch 0008 to depricated list (patch will break code if runned in later versions) *(Daphne Witmer)*


## 2.138.1 (2020-08-12)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.138.1)

*  [IMPROVEMENT] Added deprecated version for 0138 patch *(Collin Woerde)*


## 2.138.0 (2020-08-12)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.138.0)

*  [FEATURE][VKT-212] Added patch to fix area_code_already_set_error_when_catalog_images_resize 2.3.4 EE *(Collin Woerde)*


## 2.137.0 (2020-08-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.137.0)

*  [FEATURE] Add patch to fix escaping pager limit url in 2.3.3 fixed in 2.4 *(Jordy Pouw)*


## 2.136.1 (2020-08-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.136.1)

*  [FEATURE] added deprecated.json *(dheesbeen)*


## 2.136.0 (2020-08-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.136.0)

*  [FEATURE] Added patch to remove unnecessary less variable in the Magento_Company Module for Magento_NegotiableQuote variable *(Lewis Voncken)*


## 2.0.142 (2020-08-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.142)

*  [FEATURE] [DRL-730] Added patch so only the requested image is resized through the frontend *(Lewis Voncken)*


## 2.0.141 (2020-08-10)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.141)

*  [FEATURE] Added patch to remove unnecessary dependency for Magento_Negotiable Quote *(Lewis Voncken)*


## 2.0.140 (2020-08-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.140)

*  [BUGFIX][VBK-878|SGG-262] added patch for multisafepay for orders that arent closed after payment success *(Thomas Mondeel)*


## 2.0.139 (2020-08-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.139)

*  [FEATURE][SEDG-38] - Patch to prevent extremely long loading time when combining filters on category page *(TonMatton)*


## 2.0.138 (2020-07-31)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.138)

*  [BUGFIX][SCNR-162] Prevent newsletter unsubscribe email on account creation - Enable successful newsletter subscription after email-confirmation *(Bas van der Louw)*


## 2.0.137 (2020-07-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.137)

*  [BUGFIX] fixed bug where category images is not being saved in 2.3.5 *(Martijn van Haagen)*


## 2.0.136 (2020-07-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.136)

*  [FEATURE] Added patch for Undefined class constant XML_PATH_HEADER_TRANSLATE_TITLE in 2.4.0 *(René Schep)*


## 2.0.135 (2020-07-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.135)

*  [FEATURE] Renamed double patch *(martijnvanhaagen)*


## 2.0.134 (2020-07-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.134)

*  [BUGFIX] fixed error on storeview save on category url rewrite aftersave. Error: Argument 2 passed to Magento\CatalogUrlRewrite\Model\Category\Plugin\Store\View::afterSave() must be an instance of Magento\Store\Model\ResourceModel\Store, null given *(martijnvanhaagen)*


## 2.0.133 (2020-07-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.133)

*  Move and refactored duplicate patch #124 *(Bas van der Louw)*


## 2.0.132 (2020-07-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.132)

*  [BUGFIX][PSTR-122] Fixes creation of new customer account from frontend with required DOB field *(Bas van der Louw)*


## 2.0.131 (2020-07-28)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.131)

*  [DOCS] Updated readme with 100 char file limit notice README.md edited online with Bitbucket *(Collin Woerde)*
*  0114_fix_area_code_not_set_xtento_custom_attributes_2.3.4_CE.patch edited online with Bitbucket ( fixed line indentations ) *(Jasper van den Berg)*


## 2.0.130 (2020-07-27)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.130)

*  [BUGFIX] Filename to long for tar, Fixed blank page when email preview is used in admin (2.3.4 CE), fixed in magento 2.3.5 *(Collin Woerde)*
*  [BUGFIX] Removed wrong file *(Collin Woerde)*


## 2.0.129 (2020-07-27)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.129)

*  [BUGFIX][RPM2-152] Fixed blank page when email preview is used in admin (2.3.4 CE), fixed in magento 2.3.5 *(Collin Woerde)*


## 2.0.128 (2020-07-27)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.128)

*  [BUGFIX] fix for multiple duplicate images on product duplicate *(martijnvanhaagen)*


## 2.0.127 (2020-07-24)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.127)

*  [BUGFIX][SM2-1219] Customer Rest Api fixed overwrite of customer group id to default value when no customer group id is defined in rest call *(Derrick Heesbeen)*


## 2.0.126 (2020-07-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.126)

*  [TASK] [SCSUP-937] Added patch to force config reload after the cache is cleaned to prevent incorrect configuration in admin *(Lewis Voncken)*


## 2.0.125 (2020-07-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.125)

*  [BUGFIX][NENM-430] Add missing setAdditionalInformation in OrderPaymentInterface *(dulshad)*


## 2.0.123 (2020-06-19)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.123)



## 2.0.124 (2020-06-19)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.124)

*  [FEATURE] [STJ-743] Added Patch for zero / 0 indexed price for configurable products *(Lewis Voncken)*


## 2.0.122 (2020-06-17)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.122)

*  [BUGFIX][SPC-1956] - add $this->customerIsGuest(false) to addCustomer method on the Quote model *(Cas Satter)*


## 2.0.121 (2020-06-15)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.121)

*  [FEATURE] [SCSUP-936] Added patch to solve invalid cart items in admin customer tab *(Lewis Voncken)*


## 2.0.120 (2020-06-15)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.120)

*  [REFACTOR] Change patch name to reflect fix (tax class fix for i.e. Oculus). *(Boris van Katwijk)*
*  [BUGFIX][DDIJK-138] added patch for magento shopby yes/no attribute support *(thomas mondeel)*


## 2.0.119 (2020-06-10)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.119)

*  [BUGFIX][POCO-1378] - Create patch 117 to enforce PostNL tax conversion when needed (in combination with patch 108) *(Cas Satter)*


## 2.0.118 (2020-06-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.118)

*  [BUGFIX][BULT-194] - Fix Oculus realtime prices bug: price in cart was original price instead of custom price *(TonMatton)*
*  Added vendor to filepath of file to patch *(TonMatton)*


## 2.0.117 (2020-06-05)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.117)

*  [BUGFIX] fix url rewrite category *(martijnvanhaagen)*


## 2.0.116 (2020-06-04)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.116)

*  [BUGFIX] renamed patch because file nane to long for tar extract on some envirements *(thomas mondeel)*


## 2.0.115 (2020-06-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.115)

*  0114_fix_area_code_not_set_xtento_custom_attributes_2.3.4_CE.patch created online with Bitbucket *(Jasper van den Berg)*


## 2.0.114 (2020-05-28)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.114)

*  [BUGFIX] Added extra whiteline at end of file. *(René Schep)*


## 2.0.113 (2020-05-28)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.113)

*  [TASK] Added Patch that enables order emails for GraphQl *(René Schep)*


## 2.0.105 (2020-05-27)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.105)

*  [BUGFIX][POCO-1350] - Create patch to fix category URL patchs for Magento 2.3.4 when using subcategories & multilanguage storeviews *(Cas Satter)*


## 2.0.104 (2020-05-20)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.104)

*  [BUGFIX] added patch for bug in multisafepay about EI. https://github.com/MultiSafepay/Magento2Msp/pull/121 *(thomas mondeel)*


## 2.0.103 (2020-05-20)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.103)

*  [FEATURE][SUPRO-524] added patch for Tag date invalid in Entity Exception *(thomas mondeel)*


## 2.0.102 (2020-05-14)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.102)

*  [BUGFIX] - Create patch to allow Sitemap generation plugins from 2.2.X to still be used for 2.3.X *(Cas Satter)*


## 2.0.101 (2020-05-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.101)

*  [BUGFIX][POCO-1243] - Create patch 108 to fix Tax conversion for Shipping prices *(Cas Satter)*


## 2.0.100 (2020-05-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.100)

*  0107_fix_date_invoice_pdf_2.3.4_CE_COMPOSER.patch created online with Bitbucket *(Jasper van den Berg)*


## 2.0.99 (2020-05-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.99)

*  [REFACTOR] - Improve patch name to clarify its for onestepcheckout *(Cas Satter)*
*  [DOCS] Updated the CHANGELOG.md *(Cas Satter)*


## 2.0.98 (2020-05-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.98)

*  [BUGFIX] - Further improve patch to prevent checkout address issues *(Cas Satter)*


## 2.0.97 (2020-05-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.97)

*  [BUGFIX] Fixed bug where Magento in checkout sets loggedin customer as quest *(Martijn van Haagen)*


## 2.0.96 (2020-05-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.96)

*  [BUGFIX][POCO-1274] - Improve patch 86 as to prevent an empty invoice *(Cas Satter)*
*  [DOCS] Updated the CHANGELOG.md *(Cas Satter)*


## 2.0.95 (2020-05-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.95)

*  [BUGFIX] Added check in Magento Checkout js files for custom attributes , fixed in Magento 2.4 https://github.com/magento/magento2/commit/6d985af3486280a5a672ef32681a6cdb2a148aa3 *(martijnvanhaagen)*


## 2.0.94 (2020-05-04)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.94)

*  [BUGFIX] added number to patch in order to run it *(thomas mondeel)*


## 2.0.93 (2020-05-04)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.93)

*  [BUGFIX] added patch for product attribute loading in rules. solves issue with whitescreen in the checkout *(thomas mondeel)*


## 2.0.92 (2020-04-28)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.92)

*  [BUGFIX][SUPRO-500] Fix infinite loop resulting in crash when trying to save-and-duplicate catalog_product in backend - Fixed in 2.4 *(Bas van der Louw)*


## 2.0.91 (2020-04-23)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.91)

*  Added leading 0's to all filenames for clarity *(Rens Wolters)*


## 2.0.90 (2020-04-21)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.90)

*  [PATCH] patch for grouped product where 1 option is sold out *(Martijn van Haagen)*


## 2.0.89 (2020-04-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.89)

*  [BUGFIX] - Fix when adding new product attribute option/swatch, options match properly, so no existing swatch values are overriden, and new value is saved correctly *(Ruben Panis)*


## 2.0.88 (2020-04-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.88)

*  [BUGFIX] - Fixed namespace configuration of ExpressionConvertor *(Rens Wolters)*


## 2.0.87 (2020-04-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.87)

*  [BUGFIX][SXLMP-225] - Fixed database name too long for cron lock. FIXED IN 2.3.5.  https://github.com/magento/magento2/issues/22240 <- Fixes this issue, applied Magento patches *(Rens Wolters)*


## 2.0.86 (2020-04-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.86)

*  [BUGFIX] PayPal Express Checkout issue with region patch for Magento 2.3.4 *(Martijn van Haagen)*


## 2.0.85 (2020-03-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.85)

*  [BUGFIX][GKB-700] - Made patch 87 compatible with Magento 2.3.4 *(Ton Matton)*


## 2.0.84 (2020-03-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.84)

*  [BUGFIX] Made 2.3.4 compatible version of patch 84 *(Matthijs Breed)*


## 2.0.83 (2020-03-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.83)

*  [BUGFIX][SBIO-332] - Fixed undefined offset error on magento framework filter directiveprocessore dependdirective *(Rens Wolters)*


## 2.0.82 (2020-03-26)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.82)

*  [BUGFIX][SXLMP-151] - Fixed amasty base helper utils not found error on compile. Code was given by Amasty. *(Rens Wolters)*


## 2.0.81 (2020-03-17)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.81)

*  [BUGFIX][POCO-1075] - Create patch to allow creation of product image attributes *(Cas Satter)*


## 2.0.80 (2020-03-10)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.80)

*  [BUGFIX] - Fix DPD product name filtering when the special character is the unlucky character to be substr()'d *(Cas Satter)*


## 2.0.79 (2020-03-09)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.79)

*  [BUGFIX] [EVM2-139] Magento 2.3 compatibility patch for Rubic Clean Checkout Theme *(Matthijs Breed)*


## 2.0.78 (2020-03-09)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.78)

*  [BUGFIX] Fixed patch 84 *(Matthijs Breed)*


## 2.0.77 (2020-03-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.77)

*  [BUGFIX] [STEVU-338] Fixed bug where a new product was not generating url-rewrites in single store mode *(Matthijs Breed)*


## 2.0.76 (2020-03-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.76)

*  [BUGFIX][BOOTS-310] Fix spreadsheet processor of xtento stock import *(Dulshad Faraj)*


## 2.0.75 (2020-03-04)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.75)

*  [BUGFIX] - Fix customer_grid index when importing customers *(Cas Satter)*


## 2.0.74 (2020-02-25)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.74)

*  [BUGFIX][BOOTS-372] - Create patch to prevent category images from disappearing on category save *(Cas Satter)*


## 2.0.73 (2020-02-20)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.73)

*  [BUGFIX] Fixed patch path to correct path from root *(Bas van der Louw)*


## 2.0.72 (2020-02-20)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.72)

*  [BUGFIX] - Fix https://github.com/magento/magento2/issues/24131 (no payment methods show when customer has multiple addresses but no default shipping address) *(Cas Satter)*


## 2.0.71 (2020-02-20)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.71)

*  [BUGFIX] Fix off-centered checkboxes in admin grid (cache management/index management) *(Bas van der Louw)*


## 2.0.70 (2020-02-14)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.70)

*  [PATCH] - Create patch to prevent Magento locking consumers / queue messages *(Cas Satter)*


## 2.0.69 (2020-01-21)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.69)

*  83_amasty_base_1.9.7_unserialize_fallback_fix_CE_2.3.3.patch created online with Bitbucket *(Jasper van den Berg)*


## 2.0.68 (2020-01-17)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.68)

*  [BUGFIX] - Fix patch 78 with code to change *(Cas Satter)*


## 2.0.67 (2020-01-14)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.67)

*  82_EmailMessageInterface_2.3.3_backward_compatibility_github-2019-10-14-03-45-36.patch created online with Bitbucket *(René Schep)*


## 2.0.66 (2020-01-14)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.66)

*  [BUGFIX][TWM2-272] - Fix for magento2:export-product(s) when adding new value to textual swatch *(TonMatton)*
*  [BUGFIX][TWM2-272] - Changed path to correct file *(TonMatton)*


## 2.0.65 (2020-01-09)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.65)

*  [BUGFIX] - Fix for Helper static attribute when the class has a plugin on it *(Matthijs Breed)*


## 2.0.64 (2020-01-09)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.64)

*  [FEATURE] Added patch fix for bug template vars in transportbuilder magento 2.3.3 *(Martijn van Haagen)*
*  [BUG] fixed path patch *(Martijn van Haagen)*


## 2.0.62 (2020-01-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.62)

*  [FEATURE] - Port Enterprise patch from Magento to patcher module *(Cas Satter)*


## 2.0.61 (2019-12-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.61)

*  [TASK][SCSUP-803] Fixed issue where groupmanagement resets ui component sorting order *(Egor Dmitriev)*
*  [BUGFIX] - Fixes NotFoundException from Amasty Base *(Rens Wolters)*


## 2.0.60 (2019-12-19)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.60)

*  [BUGFIX] - Port Magento created patch to patcher module, Pagebuilder will now load minified JS files in production *(Cas Satter)*


## 2.0.59 (2019-12-18)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.59)

*  [BUGFIX] - Set required version for require-js to * *(Cas Satter)*


## 2.0.58 (2019-12-18)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.58)

*  README.md edited online with Bitbucket to indicate people should update our Magento upgrade knowledge base when creating patches that fixes a Magento issue. *(Cas Satter)*
*  [BUGFIX] - Create patch for https://github.com/magento/magento2/issues/25068 *(Cas Satter)*


## 2.0.57 (2019-12-13)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.57)

*  [FEATURE] - Add patch 73 to fix emails being opened as an attachment when using an email client *(Cas Satter)*


## 2.0.56 (2019-12-12)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.56)

*  [BUGFIX] Sales Order History Blocks sets page title regardless of route *(Matthijs Breed)*


## 2.0.55 (2019-12-05)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.55)

*  [BUGFIX] FIxed faulty indenting *(René Schep)*


## 2.0.54 (2019-12-05)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.54)

*  [BUGFIX] Catalog Product Price indexer performace fix *(Matthijs Breed)*


## 2.0.53 (2019-12-03)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.53)

*  70_EmailMessageInterface_backward_compatibility_CE_2.3.3.patch created online with Bitbucket *(Jasper van den Berg)*


## 2.0.52 (2019-12-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.52)

*  [BUGFIX] - Create patch to prevent Session->getQuote to fill exception.log with noncritical exceptions *(Matthijs Breed)*


## 2.0.51 (2019-11-28)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.51)

*  [BUGFIX] [MSBS-38] - Create patch to fix error in Magento Enterprise Google Tag Manager module when dealing with empty product collections *(Matthijs Breed)*


## 2.0.50 (2019-11-25)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.50)

*  [BUGFIX][RM-216] - Create patch to fix customer address backend edit country select *(Cas Satter)*


## 2.0.49 (2019-10-17)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.49)

*  Fixes save of custom options in backend when locale is set to NL for account, and scope is set to a NL store (i.e. its locale = NL) for product data. Same goes for any other locale that uses "," as decimal symbol. *(Jasper van den Berg)*


## 2.0.48 (2019-09-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.48)

*  [BUGFIX] STEVU-300 Edit patch to also fix issue for guest customers *(Matthijs Breed)*


## 2.0.47 (2019-08-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.47)

*  [FEATURE] STEVU-285 Added values for tier prices of configurable products without VAT *(Matthijs Breed)*


## 2.0.46 (2019-08-26)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.46)

*  [TASK][SCSUP-769] Fixed wysiwyg editor unable to deserialize video / image information *(Egor Dmitriev)*


## 2.0.45 (2019-08-19)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.45)

*  [TASK] STEVU-272 Added 63_Check_Order_is_virtal_before_saving_shipping_address_2.2.9_CE_COMPOSER.patch *(Matthijs Breed)*


## 2.0.44 (2019-08-05)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.44)

*  [TASK] STEVU-272 Refactored patches 36 and 45 for Magento 2.2.9 *(Matthijs Breed)*


## 2.0.43 (2019-08-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.43)

*  [TASK] - Change sortorder from 30 -> 40 *(Cas Satter)*


## 2.0.42 (2019-07-17)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.42)

*  [TASK] - POCO-338 - Create Amasty Shopby 2.12.5 patch to fix category id parameter in UrlBuilder *(Cas Satter)*


## 2.0.41 (2019-06-25)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.41)

*  [TASK] Added 59_fix_from_adress_email_2.2.8_CE_COMPOSER.patch *(Daphne Witmer)*


## 2.0.40 (2019-06-24)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.40)

*  18_min_order_amount-MDVA-3740_CE_2.1.4_COMPOSER_v1.patch edited online with Bitbucket *(gijs kwaks)*


## 2.0.39 (2019-06-21)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.39)

*  fixes_problem_getProductUrlBackendScope_CE_COMPOSER.patch created online with Bitbucket *(Mohammed Lamrani)*
*  patches/fixes_problem_getProductUrlBackendScope_CE_COMPOSER.patch edited online with Bitbucket *(Mohammed Lamrani)*
*  fixes_problem_getProductUrlBackendScope_CE_COMPOSER.patch created online with Bitbucket *(Mohammed Lamrani)*
*  patches/fixes_problem_getProductUrlBackendScope_CE_COMPOSER.patch edited online with Bitbucket *(Mohammed Lamrani)*
*  58_fixes_problem_getProductUrlBackendScope_CE_COMPOSER.patch created online with Bitbucket *(Mohammed Lamrani)*
*  58_patches/fixes_problem_getProductUrlBackendScope_CE_COMPOSER.patch edited online with Bitbucket *(Mohammed Lamrani)*
*  patches/58_fix_getProductUrlBackendScope_CE_COMPOSER.patch edited online with Bitbucket *(Mohammed Lamrani)*
*  patches/58_getProductUrlBackendScope_CE_COMPOSER.patch edited online with Bitbucket *(Mohammed Lamrani)*
*  58_getProductUrlBackendScope_CE_COMPOSER.patch edited online with Bitbucket *(Mohammed Lamrani)*
*  18_min_order_amount-MDVA-3740_CE_2.1.4_COMPOSER_v1.patch edited online with Bitbucket *(gijs kwaks)*


## 2.0.38 (2019-06-03)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.38)

*  [TASK] - POCO-332 - Create patch to change sort order in PostcodeCheck of TIG PostNL *(Cas Satter)*
*  [BUGFIX] - POCO-332 - Fix diff path in patch *(Cas Satter)*


## 2.0.37 (2019-05-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.37)

*  [TASK] SFTG-53 Added patch for fix ENGCOM-1516: fix: do not set forced are in template *(Matthijs Breed)*


## 2.0.36 (2019-05-13)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.36)

*  Fix on missing newline in patch 55 *(Matthijs Breed)*


## 2.0.35 (2019-04-16)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.35)

*  [TASK] SMUS-145 Created patch for Myparcel module to not crash on street with virtual order *(Matthijs Breed)*


## 2.0.34 (2019-04-05)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.34)

*  42_fix_default_sorting_behaviour_product_position_2.2.4_CE.patch *(Daphne Witmer)*
*  patches/54_fix_default_sorting_behaviour_product_position_2.2.4_CE.patch edited online with Bitbucket *(Daphne Witmer)*


## 2.0.33 (2019-03-16)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.33)

*  [TASK] Solved MEQ2 Magento Scan Error *(Lewis Voncken)*


## 2.0.32 (2019-03-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.32)

*  [TASK] - Issue-3 - Add comment in regards of context when patching code that is at the end of a file *(Cas Satter)*


## 2.0.31.1 (2019-03-04)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.31.1)

*  [TASK] - PGW-119 - Starting over with patch due to errors *(Cas Satter)*
*  [BUGFIX] - PGW-119 - Fix patch *(Cas Satter)*


## 2.0.31 (2019-03-04)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.31)

*  [TASK] - PGW-119 - Patch to properly apply (default) billing address in quote *(Cas Satter)*
*  [TASK] - Remove quote.billingAddress set Billing Address as method will return the shipping address (doesn't check address type in quote) *(Cas Satter)*


## 2.0.30 (2019-02-26)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.30)

*  [TASK] Added patch to fix saving product with tier prices on store view level *(Daphne Witmer)*


## 2.0.29 (2019-02-22)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.29)

*  [TASK][SCSUP-50] Fixed issue where multiple Wysiwyg Editor widgets use same modal, but don't switch context. Causing only one to work. *(Egor Dmitriev)*
*  [TASK][SCSUP-50] Fixed issue where multiple Wysiwyg Editor widgets use same modal, but don't switch context. Causing only one to work. *(Egor Dmitriev)*


## 2.0.28 (2019-01-14)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.28)

*  Added patch 50_reset_password_fix_2.2.6_CE_COMPOSER.patch *(Jasper van den Berg)*


## 2.0.27 (2018-12-20)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.27)

*  [TASK] Patch added for 2.2.4 Enterprise (Commerce). Configurable product price of out of stock products does not show on category page. *(Boris van Katwijk)*


## 2.0.26 (2018-12-17)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.26)

*  [TASK] Added TRANSSMART patch for reversed table rate issue. *(Jasper van den Berg)*


## 2.0.25 (2018-12-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.25)

*  [FEATURE] -  Add patch for category metatitle, makes sure it falls back to category name when metatitle is empty *(Ruben Panis)*


## 2.0.24 (2018-12-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.24)

*  [FEATURE] - Add patch for product metatitle, makes sure it falls back to product name when metatitle is empty, fixed in 2.2.6 *(Ruben Panis)*


## 2.0.23 (2018-10-03)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.23)

*  [FEATURE] - Makes sure the cart is not emptied after password reset by customer (after setting new password) *(Ruben Panis)*


## 2.0.22 (2018-09-04)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.22)

*  Added 44_fixes_no_results_on_filter_2.1.14_CE_COMPOSER.patch *(René Schep)*


## 2.0.21 (2018-08-22)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.21)

*  [FEATURE] - Add patch to fix exception on wishlist when product has price 0 *(Ruben Panis)*


## 2.0.20 (2018-08-17)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.20)

*  42_prevent_store_switcher_from_302_redirect_to_him_self_CE_COMPOSER.patch edited online with Bitbucket *(Robbert Stevens)*


## 2.0.19 (2018-08-16)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.19)

*  42_prevent_store_switcher_from_302_redirect_to_him_self_CE_COMPOSER.patch edited online with Bitbucket *(Robbert Stevens)*


## 2.0.18 (2018-08-16)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.18)

*  42_prevent_store_switcher_from_302_redirect_to_him_self_CE_COMPOSER.patch edited online with Bitbucket *(Robbert Stevens)*


## 2.0.17 (2018-08-16)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.17)

*  [TASK] Added patch for bug with the configurable products import through csv when the configurable attribute values contains spaces *(Lewis Voncken)*
*  report_json_error_to_user_2.1.8_CE_COMPOSER.patch created online with Bitbucket *(wilbeus)*
*  printorder_CE_2.1.8_COMPOSER.patch created online with Bitbucket *(Jeroen Coppelmans)*
*  printorder_CE_2.1.8_COMPOSER.patch add blank line on end of file *(Jeroen Coppelmans)*
*  [TASK] Add patch for custom option, all values being deleted if one earlier is deleted *(Bart Lubbersen)*
*  [BUGFIX] Added more context to patch so it will only be applied once *(Bart Lubbersen)*
*  README.md edited online with Bitbucket *(Lewis Voncken)*
*  [TASK] Added patch to fix the update/replace of images through the API *(Lewis Voncken)*
*  [TASK] Added patch for Missing Products in frontend after Full Price Index *(Lewis Voncken)*
*  Update readme *(Robbert Stevens)*
*  [FEATURE] Added customer grid indexer patch for Magento 2.2 where indexer was crashing on missing function. *(Boris van Katwijk)*
*  README.md edited online with Bitbucket *(Lewis Voncken)*
*  [BUGFIX] Fixes customer API issue where the customer group is set to General by default. *(Wil van Beusekom)*
*  fixes_customer_api_bug_with_general_group_2.1.10_CE_COMPOSER.patch edited online with Bitbucket *(wilbeus)*
*  fix_broken_wysiwyg_editor_image_links.patch created online with Bitbucket *(Peter Keijsers)*
*  fix_broken_wysiwyg_editor_image_links.patch edited online with Bitbucket *(Peter Keijsers)*
*  [TASK] Renamed file according to convention *(Lewis Voncken)*
*  [TASK] Customer Grid Indexer Broken after upgrading to 2.2.2 *(Peter Keijsers)*
*  [TASK] Add Flat Product Index Invalid / 0 row_id for EE *(Lewis Voncken)*
*  [BUGIFX] Added v2 version of fixes_customer_api_bug_with_general_group *(René Schep)*
*  [TASK] Added Index of the Patch as Prefix requirement - Doesn't apply when Cutom Module Option is specified *(Lewis Voncken)*
*  [BUGFIX] Renamed patch file because it is to long *(Lewis Voncken)*
*  [TASK] Added patch for issue with Paypal Express not working due to not sending terms and agreements with the request after clicking on the 'go to paypal' button. Shows an error message in the frontend: *(Peter Keijsers)*
*  [TASK] Fixed paths from app/code/magento/Paypal to vendor/magento/module-paypal *(Peter Keijsers)*
*  [TASK] - Added patch to fix Area code not set error when changing admin user password with cli for EE with B2B. Disables name change check plugin for negotiable quote module *(Ruben Panis)*
*  [FEATURE] Added new Patch to stop Rest Api from making unused php sessions every call *(René Schep)*
*  [TASK] Removed headers from patch file so it does have a hunk fail *(René Schep)*
*  [FEATURE] - Added patch to make it able to filter on null value for attribute when filtering collection, will be fixed in 2.3 *(Ruben Panis)*
*  [FEATURE] 2.1 Remove duplicate empty value from country drop down. *(René Schep)*
*  [TASK] Updated patch to also work if $topCountryCodes wasn't set *(René Schep)*
*  [BUGFIX] Made path relative to root for patch 37 *(René Schep)*
*  [BUGFIX] Fixed patch 37 from not applying correctly *(René Schep)*
*  [BUGFIX] Patch 37 Add to array instead of rewriting it. *(René Schep)*
*  [TASK] added new patch 38_update_fetching_quote_item_by_id_2.1_CE_COMPOSER *(Jeroen Coppelmans)*
*  [TASK] fixed path to patch file in patch 38 *(Jeroen Coppelmans)*
*  [FEATURE] - Add patch to remove page title for customer login. Can be used if block is injected elsewhere. Also see https://github.com/magento/magento2/issues/13982 *(Ruben Panis)*
*  [TASK] - Add patch to fix quote not found error in negotiable quote list when product in quote is deleted in EE B2B *(Ruben Panis)*
*  41_fix_gallery_caption_setting_view-xml_never_evaluated_2.2.5_CE_EE.patch created online with Bitbucket *(Peter Keijsers)*
*  min_order_amount-MDVA-3740_CE_2.1.4_COMPOSER_v1.patch edited online with Bitbucket *(Derrick Heesbeen)*
*  42_prevent_store_switcher_from_302_redirect_to_him_self_CE_COMPOSER.patch created online with Bitbucket *(Robbert Stevens)*


## 1.10.2 (2018-08-02)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.10.2)

*  [TASK] Added patch for bug with the configurable products import through csv when the configurable attribute values contains spaces *(Lewis Voncken)*
*  report_json_error_to_user_2.1.8_CE_COMPOSER.patch created online with Bitbucket *(wilbeus)*
*  printorder_CE_2.1.8_COMPOSER.patch created online with Bitbucket *(Jeroen Coppelmans)*
*  printorder_CE_2.1.8_COMPOSER.patch add blank line on end of file *(Jeroen Coppelmans)*
*  [TASK] Add patch for custom option, all values being deleted if one earlier is deleted *(Bart Lubbersen)*
*  [BUGFIX] Added more context to patch so it will only be applied once *(Bart Lubbersen)*
*  README.md edited online with Bitbucket *(Lewis Voncken)*
*  [TASK] Added patch to fix the update/replace of images through the API *(Lewis Voncken)*
*  [TASK] Added patch for Missing Products in frontend after Full Price Index *(Lewis Voncken)*
*  Update readme *(Robbert Stevens)*
*  [FEATURE] Added customer grid indexer patch for Magento 2.2 where indexer was crashing on missing function. *(Boris van Katwijk)*
*  README.md edited online with Bitbucket *(Lewis Voncken)*
*  [BUGFIX] Fixes customer API issue where the customer group is set to General by default. *(Wil van Beusekom)*
*  fixes_customer_api_bug_with_general_group_2.1.10_CE_COMPOSER.patch edited online with Bitbucket *(wilbeus)*
*  fix_broken_wysiwyg_editor_image_links.patch created online with Bitbucket *(Peter Keijsers)*
*  fix_broken_wysiwyg_editor_image_links.patch edited online with Bitbucket *(Peter Keijsers)*
*  [TASK] Renamed file according to convention *(Lewis Voncken)*
*  [TASK] Customer Grid Indexer Broken after upgrading to 2.2.2 *(Peter Keijsers)*
*  [TASK] Add Flat Product Index Invalid / 0 row_id for EE *(Lewis Voncken)*
*  [BUGIFX] Added v2 version of fixes_customer_api_bug_with_general_group *(René Schep)*
*  [TASK] Added Index of the Patch as Prefix requirement - Doesn't apply when Cutom Module Option is specified *(Lewis Voncken)*
*  [BUGFIX] Renamed patch file because it is to long *(Lewis Voncken)*
*  [TASK] Added patch for issue with Paypal Express not working due to not sending terms and agreements with the request after clicking on the 'go to paypal' button. Shows an error message in the frontend: *(Peter Keijsers)*
*  [TASK] Fixed paths from app/code/magento/Paypal to vendor/magento/module-paypal *(Peter Keijsers)*
*  [TASK] - Added patch to fix Area code not set error when changing admin user password with cli for EE with B2B. Disables name change check plugin for negotiable quote module *(Ruben Panis)*
*  [FEATURE] Added new Patch to stop Rest Api from making unused php sessions every call *(René Schep)*
*  [TASK] Removed headers from patch file so it does have a hunk fail *(René Schep)*
*  [FEATURE] - Added patch to make it able to filter on null value for attribute when filtering collection, will be fixed in 2.3 *(Ruben Panis)*
*  [FEATURE] 2.1 Remove duplicate empty value from country drop down. *(René Schep)*
*  [TASK] Updated patch to also work if $topCountryCodes wasn't set *(René Schep)*
*  [BUGFIX] Made path relative to root for patch 37 *(René Schep)*
*  [BUGFIX] Fixed patch 37 from not applying correctly *(René Schep)*
*  [BUGFIX] Patch 37 Add to array instead of rewriting it. *(René Schep)*
*  [TASK] added new patch 38_update_fetching_quote_item_by_id_2.1_CE_COMPOSER *(Jeroen Coppelmans)*
*  [TASK] fixed path to patch file in patch 38 *(Jeroen Coppelmans)*
*  [FEATURE] - Add patch to remove page title for customer login. Can be used if block is injected elsewhere. Also see https://github.com/magento/magento2/issues/13982 *(Ruben Panis)*
*  [TASK] - Add patch to fix quote not found error in negotiable quote list when product in quote is deleted in EE B2B *(Ruben Panis)*
*  41_fix_gallery_caption_setting_view-xml_never_evaluated_2.2.5_CE_EE.patch created online with Bitbucket *(Peter Keijsers)*
*  min_order_amount-MDVA-3740_CE_2.1.4_COMPOSER_v1.patch edited online with Bitbucket *(Derrick Heesbeen)*


## 2.0.16 (2018-07-26)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.16)

*  41_fix_gallery_caption_setting_view-xml_never_evaluated_2.2.5_CE_EE.patch created online with Bitbucket *(Peter Keijsers)*


## 2.0.15 (2018-07-13)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.15)

*  [TASK] - Add patch to fix quote not found error in negotiable quote list when product in quote is deleted in EE B2B *(Ruben Panis)*


## 2.0.14 (2018-06-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.14)

*  [FEATURE] - Add patch to remove page title for customer login. Can be used if block is injected elsewhere. Also see https://github.com/magento/magento2/issues/13982 *(Ruben Panis)*


## 2.0.13 (2018-06-25)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.13)

*  [TASK] fixed path to patch file in patch 38 *(Jeroen Coppelmans)*


## 2.0.12 (2018-06-25)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.12)

*  [TASK] added new patch 38_update_fetching_quote_item_by_id_2.1_CE_COMPOSER *(Jeroen Coppelmans)*


## 2.0.11 (2018-06-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.11)

*  [BUGFIX] Patch 37 Add to array instead of rewriting it. *(René Schep)*


## 2.0.10 (2018-06-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.10)

*  [BUGFIX] Fixed patch 37 from not applying correctly *(René Schep)*


## 2.0.9 (2018-06-06)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.9)

*  [BUGFIX] Made path relative to root for patch 37 *(René Schep)*


## 2.0.8 (2018-06-05)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.8)

*  [TASK] Updated patch to also work if $topCountryCodes wasn't set *(René Schep)*


## 2.0.7 (2018-06-04)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.7)

*  [FEATURE] 2.1 Remove duplicate empty value from country drop down. *(René Schep)*


## 2.0.6 (2018-05-31)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.6)

*  [FEATURE] - Added patch to make it able to filter on null value for attribute when filtering collection, will be fixed in 2.3 *(Ruben Panis)*


## 2.0.5 (2018-04-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.5)

*  [TASK] Removed headers from patch file so it does have a hunk fail *(René Schep)*


## 2.0.4 (2018-04-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.4)

*  [FEATURE] Added new Patch to stop Rest Api from making unused php sessions every call *(René Schep)*


## 2.0.3 (2018-04-20)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.3)

*  [TASK] - Added patch to fix Area code not set error when changing admin user password with cli for EE with B2B. Disables name change check plugin for negotiable quote module *(Ruben Panis)*


## 2.0.2 (2018-04-12)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.2)

*  [TASK] Added patch for issue with Paypal Express not working due to not sending terms and agreements with the request after clicking on the 'go to paypal' button. Shows an error message in the frontend: *(Peter Keijsers)*
*  [TASK] Fixed paths from app/code/magento/Paypal to vendor/magento/module-paypal *(Peter Keijsers)*


## 2.0.1 (2018-03-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.1)

*  [BUGFIX] Renamed patch file because it is to long *(Lewis Voncken)*


## 2.0.0 (2018-03-23)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/2.0.0)

*  [TASK] Added Index of the Patch as Prefix requirement - Doesn't apply when Cutom Module Option is specified *(Lewis Voncken)*


## 1.21.0 (2018-02-09)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.21.0)

*  [BUGIFX] Added v2 version of fixes_customer_api_bug_with_general_group *(René Schep)*


## 1.20.0 (2018-01-31)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.20.0)

*  [TASK] Add Flat Product Index Invalid / 0 row_id for EE *(Lewis Voncken)*


## 1.19.0 (2018-01-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.19.0)

*  [TASK] Customer Grid Indexer Broken after upgrading to 2.2.2 *(Peter Keijsers)*


## 1.18.1 (2018-01-19)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.18.1)

*  [TASK] Renamed file according to convention *(Lewis Voncken)*


## 1.18.0 (2018-01-04)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.18.0)

*  fix_broken_wysiwyg_editor_image_links.patch created online with Bitbucket *(Peter Keijsers)*
*  fix_broken_wysiwyg_editor_image_links.patch edited online with Bitbucket *(Peter Keijsers)*


## 1.17.0 (2017-12-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.17.0)

*  fixes_customer_api_bug_with_general_group_2.1.10_CE_COMPOSER.patch edited online with Bitbucket *(wilbeus)*


## 1.16.2 (2017-12-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.16.2)

*  [BUGFIX] Fixes customer API issue where the customer group is set to General by default. *(Wil van Beusekom)*


## 1.16.1 (2017-12-13)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.16.1)

*  README.md edited online with Bitbucket *(Lewis Voncken)*


## 1.16.0 (2017-12-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.16.0)

*  Update readme *(Robbert Stevens)*
*  [FEATURE] Added customer grid indexer patch for Magento 2.2 where indexer was crashing on missing function. *(Boris van Katwijk)*


## 1.15.0 (2017-11-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.15.0)

*  [TASK] Added patch for Missing Products in frontend after Full Price Index *(Lewis Voncken)*


## 1.14.0 (2017-11-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.14.0)

*  README.md edited online with Bitbucket *(Lewis Voncken)*
*  [TASK] Added patch to fix the update/replace of images through the API *(Lewis Voncken)*


## 1.13.2 (2017-11-17)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.13.2)

*  [BUGFIX] Added more context to patch so it will only be applied once *(Bart Lubbersen)*


## 1.9.1 (2017-11-17)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.9.1)

*  [TASK] Add patch for custom option, all values being deleted if one earlier is deleted *(Bart Lubbersen)*


## 1.13.1 (2017-10-27)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.13.1)

*  printorder_CE_2.1.8_COMPOSER.patch add blank line on end of file *(Jeroen Coppelmans)*


## 1.13.0 (2017-10-27)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.13.0)

*  printorder_CE_2.1.8_COMPOSER.patch created online with Bitbucket *(Jeroen Coppelmans)*


## 1.12.0 (2017-10-12)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.12.0)

*  report_json_error_to_user_2.1.8_CE_COMPOSER.patch created online with Bitbucket *(wilbeus)*


## 1.11.0 (2017-10-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.11.0)

*  [TASK] Added patch for bug with the configurable products import through csv when the configurable attribute values contains spaces *(Lewis Voncken)*


## 1.10.0 (2017-10-04)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.10.0)

*  [TASK] Add Flat Product Created At patch *(Lewis Voncken)*


## 1.9.0 (2017-09-08)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.9.0)

*  [TASK] Add Order customer name patch *(Bart Lubbersen)*


## 1.8.0 (2017-09-07)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.8.0)

*  [TASK] Add patch for configurable products options fix through API *(Bart Lubbersen)*


## 1.7.0 (2017-09-04)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.7.0)

*  [FEATURE] Added patch to Solve problem when saving the Theme on Website level *(Lewis Voncken)*


## 1.6.0 (2017-08-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.6.0)

*  [TASK] Added patch to fix wrong sales order grid created add date for Magento  2.1.0 - 2.1.7 *(René Schep)*


## 1.5.0 (2017-08-28)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.5.0)

*  [FEATURE] Added Patch so Frontend cart should be updated after Admin edits the customer carts in admin panel *(Lewis Voncken)*


## 1.4.1 (2017-08-17)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.4.1)

*  [TASK] FIx url path category patch for 2.1.8 release *(Bart Lubbersen)*


## 1.4.0 (2017-08-17)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.4.0)

*  [FEATURE] added new patch to solve problem with always required state field in admin panel *(Lewis Voncken)*


## 1.3.2 (2017-08-16)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.3.2)

*  [TASK] renamed file for type check returns undefined *(Lewis Voncken)*


## 1.3.1 (2017-08-16)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.3.1)

*  [TASK] Patch is for EE and CE so the patch has been renamed with the reference to CE *(Lewis Voncken)*


## 1.3.0 (2017-08-16)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.3.0)

*  [FEATURE] Patch for Bug in Flat Category *(Lewis Voncken)*


## 1.2.0 (2017-08-16)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.2.0)

*  [FEATURE] added patch for Refresh Top Menu Based on Changed Customergroup incombination with Enterprise Permissions. *(Lewis Voncken)*


## 1.1.1 (2017-08-16)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.1.1)

*  [TASK] Renamed patch file *(Lewis Voncken)*


## 1.1.0 (2017-07-25)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.1.0)

*  [FEATURE] Option to apply customer specific patches (example Amasty QuickFix) *(Lewis Voncken)*


## 1.0.16 (2017-07-21)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.0.16)

*  [TASK] Added Additional patch for problem when using minimum order amount, change customer group and share account *(Lewis Voncken)*


## 1.0.15 (2017-07-11)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.0.15)

*  [TASK] Added new patch *(Lewis Voncken)*


## 1.0.14 (2017-07-10)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.0.14)

*  [TASK] Added patch *(Lewis Voncken)*


## 1.0.13 (2017-07-04)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.0.13)

*  [TASK] README.md update *(Lewis Voncken)*


## 1.0.12 (2017-07-04)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.0.12)

*  [TASK] Added new patch for Bug in Admin Panel when changing CustomerGroup if minimum order amount has been enabled and customer has active quote *(Lewis Voncken)*


## 1.0.11 (2017-06-30)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.0.11)

*  [TAKS] Updated min_order_amount_checkout_EE_COMPOSER.patch *(Lewis Voncken)*


## 1.0.10 (2017-06-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.0.10)

*  [TASK] Added Unexptected error check *(Lewis Voncken)*


## 1.0.9 (2017-06-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.0.9)

*  [TASK] Updated Checks in CLI *(Lewis Voncken)*


## 1.0.8 (2017-06-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.0.8)

*  [BUGFIX] Solved problem with can't find file check *(Lewis Voncken)*


## 1.0.7 (2017-06-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.0.7)

*  [TASK] Added Skip Enterprise function and updated README.md *(Lewis Voncken)*


## 1.0.6 (2017-06-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.0.6)

*  [TASK] Enterprise Warning and changed Patch name *(Lewis Voncken)*


## 1.0.5 (2017-06-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.0.5)

*  [TASK] Added check for missing file *(Lewis Voncken)*


## 1.0.4 (2017-06-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.0.4)

*  [TASK] Changed Available Patches List View *(Lewis Voncken)*


## 1.0.3 (2017-06-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.0.3)

*  [TASK] Added -r - so .rej files aren't created if Patch is already applied *(Lewis Voncken)*


## 1.0.2 (2017-06-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.0.2)

*  [TASK] Added Patch Status and Patch Summary *(Lewis Voncken)*


## 1.0.1 (2017-06-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.0.1)

*  [TASK] Added Patch for Undefined index: value when saving Product Status *(Lewis Voncken)*


## 1.0.0 (2017-06-29)

[View Release](https://bitbucket.org/experius/mage2-module-experius-patcher.git/commits/tag/1.0.0)

*  [TASK] Initial Commit *(Lewis Voncken)*


