Experius Patches
====================

A collection of Experius Patches, on top of the collection given by Magento.

# How to apply a patch

**Requires cweagans/composer-patches**

Add the following snippet in your composer.json

```
"extra": {
        "composer-exit-on-patch-failure": true,
        "patches-file": "composer.patches.json"
    }
```

Create the following `composer.patches.json` in your Magento root directory:

```
{
  "patches": {
    "<vendor>/<module>": {
      "<patchnumber>": "<link_to_the_patch>",
	  "<local_patch_description": "app/code/Vendor/Module/patches/name_of_the_patch.patch"
    },
    "<vendor>/<module>": {
      "<patchnumber>": "<link_to_the_patch>"
    }
  }
}
```

And update the composer.lock content-hashes:

```
composer update --lock
```

All defined patches will then be applied whenever `composer install` is executed, at the time the module the patch applies to is installed.

# Available Patches

Patches can be placed and used from anywhere (including from url's). Visit https://patches.experius.nl/ for a browsable list of all patches we currently host ourselves. However, feel free to use any patch you find available.

# Found new important patch or patch package?

We're always looking to expand our patch collection. If you discover an important patch somewhere or a whole package of patches not yet included, please share it so we can include it in our updating schedule and host it ourselves as well.

# Upgrade Patcher 2 to Patcher 3

1. Remove experius/module-patcher from composer.json
2. Install the latest version of cweagans/composer-patches
3. Add the cweagans snippets to exit on patch failure and include the composer.patches.json patches.
4. For every patch mentioned in deploy.sh, add the patch to the composer.patches.json, using the snippet above.
    - Use this webpage to have the snippet created automatically: https://patches.experius.nl/check/composer
5. Don't forget to set composer-exit-on-patch-failure on true! You won't get exit codes otherwise!
6. Remove all mentions of experius_patcher:applypatch in your deploy.sh

# Create Patch File

**Always update the knowledge base when creating a patch that fixes Magento issues!**

## Patch from Open Changes

Create a copy of the original file and add .original suffix to this copied file's name. Apply changes to the original file (The one without the .original suffix.)

Then create patch file by running the following command.
```
git diff -U10 --no-index path/to/file.original path/to/file > fixes_problem_x.x.x_CE_COMPOSER.patch
```

**Important** In the patch file remove the .original from the file paths.


It is important to have enough context to correctly apply the patch so use ``-U10`` to add 10 rows of context

Notes  
 - When patching code either at the end of a file, make sure to remove `\No newline at end of file` from the context.  
 - when creating a file never exceed the character limit of 100 for the file name itself.  

## Use PhpStorm to create a patch from open changes. This works even for vendor files.

- Make the changes you want to the file you are trying to creat a patch for.
- Right click the file in your directory tree. Select `Local History` -> `Show History`.
- Select the original version of the file on the right side and check if only changes you made are shown.
- Press the `Create patch` button in the Top Left. This creates a patch file in root of your project.
- Add it to the patches folder.
   
## Patch from Git Commit

### Commandline

Create Patch File based on the given commit by "git show"ing it for example

```
git show 617b204af7cc097e5c0f6ea568ba9532e6f3f6e3 -U10 vendor/ > fixes_problem_x.x.x_CE_COMPOSER.patch
```

Note: When patching code either at the end of a file, make sure to remove `\No newline at end of file` from the context. 
### Github

Create Patch File based on the given commit by just adding ``.patch`` behind the commit link for example

before:

```
https://github.com/poongud/magento2/commit/617b204af7cc097e5c0f6ea568ba9532e6f3f6e3
```

after:

```
https://github.com/poongud/magento2/commit/617b204af7cc097e5c0f6ea568ba9532e6f3f6e3.patch
```

### Bitbucket
Create Patch File based on the given commit by just adding ``/raw`` behind the commit link for example

before:

```
https://bitbucket.org/experius/mage2-deploy-hypernode/commits/a8a4cbbedcf39f3b38d70bacb208085a1bdf3b09
```

after:

```
https://bitbucket.org/experius/mage2-deploy-hypernode/commits/a8a4cbbedcf39f3b38d70bacb208085a1bdf3b09/raw
```


Update the Patch file if you installed Magento 2 with composer for example:

```
+++ b/app/code/Magento/CatalogInventory/Ui/DataProvider/Product/Form/Modifier/AdvancedInventory.php
```

to:

```
+++ b/vendor/magento/module-catalog-inventory/Ui/DataProvider/Product/Form/Modifier/AdvancedInventory.php
```
